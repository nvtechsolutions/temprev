@extends('layouts.app')

@section('content')
<?php
$id = (isset($template->id) ? $template->id : '');
$project_id = (isset($template->project_id) ? $template->project_id : '');


$days = [
		'Monday',
		'Tuesday',
		'Wednesday',
		'Thursday',
		'Friday',
		'Saturday',
		'Sunday'
];
$weeks = [
		'Week 01',
		'Week 02',
		'Week 03',
		'Week 04',
		'Week 05',
		'Week 06',
		'Week 07',
		'Week 08',
		'Week 09',
		'Week 10',
		'Week 11',
		'Week 12',
		'Week 13',
		'Week 14',
		'Week 15',
		'Week 16',
		'Week 17',
		'Week 18',
		'Week 19',
		'Week 20',
		'Week 21',
		'Week 22',
		'Week 23',
		'Week 24',
		'Week 25',
		'Week 26',
		'Week 27',
		'Week 28',
		'Week 29',
		'Week 30',
		'Week 31',
		'Week 32',
		'Week 33',
		'Week 34',
		'Week 35',
		'Week 36',
		'Week 37',
		'Week 38',
		'Week 39',
		'Week 40',
		'Week 41',
		'Week 42',
		'Week 43',
		'Week 44',
		'Week 45',
		'Week 46',
		'Week 47',
		'Week 48',
		'Week 49',
		'Week 50',
		'Week 51',
		'Week 52',
		'Week 53'
];
$times = [
			'00:00',
			'00:15',
			'00:30',
			'00:45',
			'01:00',
			'01:15',
			'01:30',
			'01:45',
			'02:00',
			'02:15',
			'02:30',
			'02:45',
			'03:00',
			'03:15',
			'03:30',
			'03:45',
			'04:00',
			'04:15',
			'04:30',
			'04:45',
			'05:00',
			'05:15',
			'05:30',
			'05:45',
			'06:00',
			'06:15',
			'06:30',
			'06:45',
			'07:00',
			'07:15',
			'07:30',
			'07:45',
			'08:00',
			'08:15',
			'08:30',
			'08:45',
			'09:00',
			'09:15',
			'09:30',
			'09:45',
			'10:00',
			'10:15',
			'10:30',
			'10:45',
			'11:00',
			'11:15',
			'11:30',
			'11:45',
			'12:00',
			'12:15',
			'12:30',
			'12:45',
			'13:00',
			'13:15',
			'13:30',
			'13:45',
			'14:00',
			'14:15',
			'14:30',
			'14:45',
			'15:00',
			'15:15',
			'15:30',
			'15:45',
			'16:00',
			'16:15',
			'16:30',
			'16:45',
			'17:00',
			'17:15',
			'17:30',
			'17:45',
			'18:00',
			'18:15',
			'18:30',
			'18:45',
			'19:00',
			'19:15',
			'19:30',
			'19:45',
			'20:00',
			'20:15',
			'20:30',
			'20:45',
			'21:00',
			'21:15',
			'21:30',
			'21:45',
			'22:00',
			'22:15',
			'22:30',
			'22:45',
			'23:00',
			'23:15',
			'23:30',
			'23:45'
];


if(!empty($project_schedules)){
	foreach ($project_schedules as $project_schedule) {
		if($project_schedule->entity == 'DAY'){
			foreach ($days as $day_key => $day) {
				if($project_schedule->value == $day){
					// unset($days[$day_key]);
				}
			}
		}

		if($project_schedule->entity == 'WEEK'){
			foreach ($weeks as $week_key => $week) {
				if($project_schedule->value == $week){
					// unset($weeks[$week_key]);
				}
			}
		}


		if($project_schedule->entity == 'TIME'){
			foreach ($times as $time_key => $time) {
				if($project_schedule->value == $time){
					// unset($times[$time_key]);
				}
			}
		}
	}
}
?>
<div class="page-bar">
	<ul class="page-breadcrumb">
		<li>
			<a href="index.php">Home</a>
			<i class="fa fa-circle"></i>
		</li>
		<li>
			<a href="{{ route('clients') }}">Clients</a>
			<i class="fa fa-circle"></i>
		</li>		
		<li>
			<a href="{{ route('projects', ['client_id' => $client_id]) }}">Projects</a>
			<i class="fa fa-circle"></i>
		</li>
		<li>
			<a href="{{ route('templates', ['client_id' => $client_id,'project_id' => $project_id]) }}">Templates</a>
			<i class="fa fa-circle"></i>
		</li>
		<li>
			<span>Template Scheduler</span>
		</li>
	</ul>
</div>
<!-- END PAGE BAR -->
<!-- BEGIN PAGE TITLE-->
<h1 class="page-title"> Template Scheduler
	<small>Here you can view template scheduler</small>
</h1>
<!-- END PAGE TITLE-->
<!-- END PAGE HEADER-->


<div class="row">
	<div class="col-md-offset-2 col-md-8">
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption">
					<i class="glyphicon glyphicon-calendar"></i>
					Template Scheduler
				</div>
			</div>
			<div class="portlet-body form">
				@if ($errors->any())
				    <div class="alert alert-danger">
				        <ul>
				            @foreach ($errors->all() as $error)
				                <li>{{ $error }}</li>
				            @endforeach
				        </ul>
				    </div>
				@endif
				<form action="{{ route('schedule-update-template', ['client_id' => $client_id,'id' => $id]) }}" class="form-horizontal form-row-seperated">
					<input type="hidden" name="project_id" value={{$project_id}}>
					<div class="form-body">
						<div class="form-group">
							<div class="col-md-4">
								<label class="control-label">Day of the Week</label>
								<p class="help-block">(Only display on the following days of the week)</p>
							</div>
							<div class="col-md-8">
								<a href="" id="select_all_day_week">Select All</a> / 
								<a href="" id="deselect_all_day_week">Deselect All</a>
								<select multiple="multiple" class="multi-select" id="day_of_the_week" name="day_of_the_week[]">
									@foreach($days as $day)
										<?php $selected = false; ?>
										@foreach($schedules as $schedule)
											@if($schedule->entity == 'DAY')
												@if($schedule->value == $day)
													<?php $selected = true; ?>
												@endif
											@endif
										@endforeach
										@if($selected)
											<option selected value="{{$day}}">{{$day}}</option>
										@else
											<option value="{{$day}}">{{$day}}</option>
										@endif										
									@endforeach
								</select>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-4">
								<label class="control-label">Time of the Day</label>
								<p class="help-block">(Only display at the following times of the day)</p>
							</div>
							<div class="col-md-8">
								<a href="" id="select-all-time-day">Select All</a> / 
								<a href="" id="deselect-all-time-day">Deselect All</a>
								<select multiple="multiple" class="multi-select" id="time_of_the_day" name="time_of_the_day[]">
									@foreach($times as $time)
										<?php $selected = false; ?>
										@foreach($schedules as $schedule)
											@if($schedule->entity == 'TIME')
												@if($schedule->value == $time)
													<?php $selected = true; ?>
												@endif
											@endif
										@endforeach
										@if($selected)
											<option selected value="{{$time}}">{{$time}}</option>
										@else
											<option value="{{$time}}">{{$time}}</option>
										@endif										
									@endforeach
								</select>
							</div>
						</div>
						<div class="form-group last">
							<div class="col-md-4">
								<label class="control-label">Week of the Year</label>
								<p class="help-block">(Only display on the following weeks of the year)</p>
							</div>
							<div class="col-md-8">
								<a href="" id="select-all-week-year">Select All</a> / 
								<a href="" id="deselect-all-week-year">Deselect All</a>
								<select multiple="multiple" class="multi-select" id="week_of_the_year" name="week_of_the_year[]">
									@foreach($weeks as $week)
										<?php $selected = false; ?>
										@foreach($schedules as $schedule)
											@if($schedule->entity == 'WEEK')
												@if($schedule->value == $week)
													<?php $selected = true; ?>
												@endif
											@endif
										@endforeach
										@if($selected)
											<option selected value="{{$week}}">{{$week}}</option>
										@else
											<option value="{{$week}}">{{$week}}</option>
										@endif										
									@endforeach
								</select>
							</div>
						</div>
					</div>
					<div class="form-actions right">
						<a href="{{ route('templates', ['client_id' => $client_id,'project_id' => $project_id]) }}" type="button" class="btn default">Cancel</a>
						<button type="submit" class="btn blue">Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection