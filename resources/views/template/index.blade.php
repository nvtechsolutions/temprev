@extends('layouts.app')

@section('content')

<div class="page-bar">				
	<ul class="page-breadcrumb">
		<li>
			<a href="{{ route('home') }}">Home</a>
			<i class="fa fa-circle"></i>
		</li>
		<li>
			<a href="{{ route('clients') }}">Clients</a>
			<i class="fa fa-circle"></i>
		</li>		
		<li>
			<a href="{{ route('projects', ['client_id' => $client_id]) }}">Projects</a>
			<i class="fa fa-circle"></i>
		</li>
		<li>
			<a href="{{ route('templates', ['client_id' => $client_id,'project_id' => $project_id]) }}">Templates</a>
			<i class="fa fa-circle"></i>
		</li>
		<li>
			<span>View Templates</span>
		</li>
	</ul>
</div>
<!-- END PAGE BAR -->
<!-- BEGIN PAGE TITLE-->
<h1 class="page-title"> View Templates
	<small>Here you can view all Templates</small>
</h1>
<!-- END PAGE TITLE-->
<!-- END PAGE HEADER-->
<!--<div class="note note-info">
	<p> Show operation messages here </p>
</div> -->

<div class="row">
	<div class="col-md-offset-3 col-md-6">
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-eye"></i>
					View Templates
				</div>		
				<div class="actions">		
					<a href="{{ route('add-template',['client_id' => $client_id,'project_id' => $project_id]) }}" class="btn default btn-sm">
						<i class="fa fa-plus"></i> Add Template
					</a>		
				</div>
			</div>
			<div class="portlet-body">
				<table class="table table-striped table-bordered table-hover" id="view_user_table">
					<thead>
						<tr>
							<th> Serial No</th>
							<th> Template Name </th>
							<th> Actions </th>
						</tr>
					</thead>
					<tbody>
						@foreach($templates as $template)
						<tr class="odd gradeX">
							<td>{{$template->id}}</td>
							<td>{{$template->name}}</td>
							<td>
								<a href="{{ route('edit-template', ['client_id' => $client_id,'project_id' => $project_id, 'id' => $template->id]) }}" class="btn btn-outline btn-circle btn-xs purple"><i class="fa fa-edit"></i> Edit </a>
								<a href="{{ route('schedule-template', ['client_id' => $client_id,'project_id' => $project_id, 'id' => $template->id]) }}" class="btn btn-outline btn-circle btn-xs green"><i class="glyphicon glyphicon-calendar"></i> Schedule </a>
								<a target="_blank" href="{{$template->preview_url}}" class="btn btn-outline btn-circle btn-xs blue"><i class="fa fa-search"></i> Preview </a>

								<a href="{{ route('delete-template', ['client_id' => $client_id,'project_id' => $project_id, 'id' => $template->id]) }}" class="btn btn-outline btn-circle btn-xs red delete_entity" data-toggle="modal" data-uid="1"><i class="fa fa-trash"></i> Delete </a>

							
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
				
				<div class="modal fade" id="delete_entity_modal" tabindex="-1" role="basic" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
								<h4 class="modal-title bold">Delete</h4>
							</div>
							<div class="modal-body">
								<form action="#" class="horizontal-form" id="delete_entity_form" role="form" method="get">
									<div class="form-body">
										<div class="row">
											<div class="col-md-12">
												<p>Are you sure you want to delete?</p>
											</div>                                      
										</div>
									</div>
									<div class="form-actions right" style="text-align:right;">
										<button type="submit" class="btn btn-lg red-flamingo" id="delete_entity_confirm"><i class="fa fa-check"></i> Yes</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
