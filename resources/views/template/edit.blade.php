@extends('layouts.app')

@section('content')
<?php 
$page_title = (isset($template) ? 'Edit Template' : 'Add Template');
$page_message = (isset($template) ? 'Here you can edit template' : 'Here you can create a new template');
$client_id = (isset($client_id) ? $client_id : '');
$name = (isset($template->name) ? $template->name : old('name'));
$url_option = (isset($template->url_option) ? $template->url_option : old('url_option'));
$url = (isset($template->url) ? $template->url : old('url'));
$template_boxes = (isset($template_boxes) ? $template_boxes : '');
$screen_count = (isset($screen_count) ? $screen_count : 0);

$id = (isset($template->id) ? $template->id : '');
?>

<div class="page-bar">
	<ul class="page-breadcrumb">
		<li>
			<a href="{{ route('home') }}">Home</a>
			<i class="fa fa-circle"></i>
		</li>
		<li>
			<a href="{{ route('clients') }}">Clients</a>
			<i class="fa fa-circle"></i>
		</li>
		<li>
			<a href="{{ route('projects', ['client_id' => $client_id]) }}">Projects</a>
			<i class="fa fa-circle"></i>
		</li>
		<li>
			<a href="{{ route('templates', ['client_id' => $client_id,'project_id' => $project_id]) }}">Templates</a>
			<i class="fa fa-circle"></i>
		</li>
		<li>
			<span>{{$page_title}}</span>
		</li>
	</ul>
</div>

<h1 class="page-title"> {{$page_title}}
	<small>{{$page_message}}</small>
</h1>


<div class="row">
	<div class="col-md-offset-3 col-md-6">
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption">
					<i class="glyphicon glyphicon-user"></i>
					{{$page_title}}
				</div>
			</div>			
			<div class="portlet-body form">
				@if ($errors->any())
				    <div class="alert alert-danger">
				        <ul>
				            @foreach ($errors->all() as $error)
				                <li>{{ $error }}</li>
				            @endforeach
				        </ul>
				    </div>
				@endif
				<form role="form" id="template_form" action="{{ route('update-template', ['client_id' => $client_id,'id' => $id]) }}" method="POST" enctype="multipart/form-data">
					{{ csrf_field() }}
					<input type="hidden" name="project_id" value="{{$project_id}}">
					<div class="form-body">
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label>Template Name</label>
									<input type="text" class="form-control input-sm" name="name" value="{{$name}}"> 
								</div>
								<div class="form-group">
									<div class="mt-checkbox-list">
										<label class="mt-checkbox mt-checkbox-outline"> Enable URL Option
											@if($url_option == 1)
											<input type="checkbox" checked value="1" name="url_option" id="enable_option">
											@else
											<input type="checkbox" value="1" name="url_option" id="enable_option">
											@endif
											<span></span>
										</label>
									</div>
								</div>
								<div class="form-group">
									<label>URL</label>
									<input type="text" class="form-control input-sm" name="url" value="{{$url}}" id="template_url"> 
								</div>
								<br/><br/>
								<?php
								$i=1;
								if($template_boxes != ''){
									
									foreach ($template_boxes as $template_box) {
									?>
									<input type="hidden" name="template_box_id[{{$i}}]" value="{{$template_box->id}}">
									<div class="form-group"  id="template_box_id_{{$i}}">
										<label>Box {{$i}}</label>
										<select class="form-control template_box" name="template_box[{{$i}}]" data-box-id="{{$i}}">
											<option value="">Select Option</option>

											@if($template_box->name =='Leave It Empty')
											<option selected value="Leave It Empty">Leave It Empty</option>
											@else
											<option value="Leave It Empty">Leave It Empty</option>
											@endif

											@if($template_box->name =='Upload File Image')
											<option selected value="Upload File Image">Upload a Image</option>
											@else
											<option value="Upload File Image">Upload a Image</option>
											@endif

											@if($template_box->name =='Upload File Video')
											<option selected value="Upload File Video">Upload a Video</option>
											@else
											<option value="Upload File Video">Upload a Video</option>
											@endif

											@if($template_box->name =='Add Text With a Allergy Icon')
											<option selected value="Add Text With a Allergy Icon">Add Text with Allergy Icons</option>
											@else
											<option value="Add Text With a Allergy Icon">Add Text with Allergy Icons</option>
											@endif
										</select>
									</div>
									@if($template_box->name =='' || $template_box->name =='Leave It Empty')
										<?php 
										$box_text = 'hide';
										$box_allergy = 'hide';
										$box_file_image = 'hide';
										$box_file_video = 'hide';
										?>
									@endif
									@if($template_box->name =='Add Text With a Allergy Icon')
										<?php 
										$box_text = '';
										$box_allergy = '';
										$box_file_image = 'hide';
										$box_file_video = 'hide';
										?>
									@endif
									@if($template_box->name =='Upload File Image')
										<?php 
										$box_text = 'hide';
										$box_allergy = 'hide';
										$box_file_video = 'hide';
										$box_file_image = '';
										?>
									@endif
									@if($template_box->name =='Upload File Video')
										<?php 
										$box_text = 'hide';
										$box_allergy = 'hide';
										$box_file_image = 'hide';
										$box_file_video = '';
										?>
									@endif
									<div class="form-group {{$box_text}}" id="template_box_text_{{$i}}">
										<label>Text</label>
										<input type="text" class="form-control input-sm" name="template_box_text[{{$i}}]" id="box1AllergyIcon" maxlength="38" value="{{$template_box->text}}"> 
									</div>
									<div class="form-group {{$box_allergy}}"  id="template_box_allergy_{{$i}}">
										<label>Select Allergy Icon</label>
										<select multiple="" name="template_box_allergy[{{$i}}][]" class="form-control allergy_icon_select" id="allergy_icon_box_{{$i}}">
											<?php
											$allergy_icons = explode(',', $template_box->allergy_icon);
											?>
											@foreach($aicons as $aicon)
												<?php $selected = false; ?>
												@foreach($allergy_icons as $allergy_icon)
													@if($allergy_icon == $aicon->id)
														<?php $selected = true; ?>
													@endif
												@endforeach

												@if($selected)
													<option selected value="{{$aicon->id}}">{{$aicon->name}}</option>
												@endif												
											@endforeach

											@foreach($aicons as $aicon)
												<?php $selected = false; ?>
												@foreach($allergy_icons as $allergy_icon)
													@if($allergy_icon == $aicon->id)
														<?php $selected = true; ?>
													@endif
												@endforeach

												@if(!$selected)
													<option value="{{$aicon->id}}">{{$aicon->name}}</option>
												@endif												
											@endforeach

										</select>
										<p style="color:red;" id="allergy_icon_error_{{$i}}"></p>
									</div>									
									


									<div class="form-group {{$box_file_image}}" id="template_box_file_image_{{$i}}">
										<label>File Upload - Image</label>
										<input type="file" name="template_box_file_image_{{$i}}" id="box1FileUpload" accept="image/*">
									</div>
									<div class="form-group {{$box_file_video}}" id="template_box_file_video_{{$i}}">
										<label>File Upload - Video</label>
										<input type="file" name="template_box_file_video_{{$i}}" id="box1FileUpload" accept="video/*">
									</div>

									@if($template_box->name =='Upload File Image')
									<img width="100" height="100" src="{!! url('/template/box_image/'.$template_box->file) !!}"></img>
									@endif

									@if($template_box->name =='Upload File Video')
										<?php
										$file = explode('.', $template_box->file);
										$file_format = $file[count($file)-1];
										?>
										<video width="200" controls>
										  <source src="{!! url('/template/box_video/'.$template_box->file) !!}" type="video/{{$file_format}}">
										  Your browser does not support HTML5 video.
										</video>
									@endif								
									
									<br/><br/>
									<hr/>
									<?php
									$i++;
									}
								}
								for($i=$i; $i<=$screen_count; $i++){
									?>
									<div class="form-group" id="template_box_id_{{$i}}">
										<label>Box {{$i}}</label>
										<select class="form-control template_box" name="template_box[{{$i}}]" data-box-id="{{$i}}">
											<option value="">Select Option</option>
											<option value="Leave It Empty">Leave It Empty</option>
											<option value="Upload File Image">Upload a Image</option>
											<option value="Upload File Video">Upload a Video</option>
											<option value="Add Text With a Allergy Icon">Add Text with Allergy Icons</option>
										</select>
									</div>
									<div class="form-group hide" id="template_box_text_{{$i}}">
										<label>Text</label>
										<input type="text" class="form-control input-sm" maxlength="38" name="template_box_text[{{$i}}]" id="box1AllergyIcon"> 
									</div>
									<div class="form-group hide"  id="template_box_allergy_{{$i}}">
										<label>Select Allergy Icon</label>
										<select multiple="" name="template_box_allergy[{{$i}}][]" class="form-control allergy_icon_select" id="allergy_icon_box_{{$i}}">
											@foreach($aicons as $aicon)
												<option value="{{$aicon->id}}">{{$aicon->name}}</option>
											@endforeach
										</select>
										<p id="allergy_icon_error_{{$i}}"></p>
									</div>
									<div class="form-group hide" id="template_box_file_image_{{$i}}">
										<label>File Upload - Image</label>
										<input type="file" name="template_box_file_image_{{$i}}" id="box1FileUpload" accept="image/*">
									</div>
									<div class="form-group hide" id="template_box_file_video_{{$i}}">
										<label>File Upload - Video</label>
										<input type="file" name="template_box_file_video_{{$i}}" id="box1FileUpload" accept="video/*">
									</div>
									<br/><br/>
									<hr/>
									<?php
								}
								?>
							</div>
						</div>
					</div>
					<div class="form-actions right">
						<a href="{{ route('templates', ['client_id' => $client_id,'project_id' => $project_id]) }}" type="button" class="btn default">Cancel</a>
						<button type="submit" class="btn blue">Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection