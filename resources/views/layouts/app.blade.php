<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8" />
        <title>CKC | Cyprus Kiosk Company</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="Preview page of Metronic Admin Theme #1 for blank page layout" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/global/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/global/plugins/simple-line-icons/simple-line-icons.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/global/plugins/jquery-multi-select/css/multi-select.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/global/css/components.min.css') }}" id="style_components" rel="stylesheet">
        <link href="{{ asset('assets/global/css/plugins.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/layouts/layout/css/esol_custom.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/layouts/layout/css/layout.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/layouts/layout/css/themes/darkblue.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/layouts/layout/css/custom.min.css') }}" rel="stylesheet">
        <link rel="shortcut icon" href="favicon.ico" />
</head>
<body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid">
        <div class="page-wrapper">
            <!-- BEGIN HEADER -->
            <div class="page-header navbar navbar-fixed-top">
                <!-- BEGIN HEADER INNER -->
                <div class="page-header-inner ">
                    <!-- BEGIN LOGO -->
                    <div class="page-logo">
                        <div class="menu-toggler sidebar-toggler">
                            <span></span>
                        </div>
                    </div>
                    <!-- END LOGO -->
                    <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                    <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                        <span></span>
                    </a>
                    <!-- END RESPONSIVE MENU TOGGLER -->
                    <!-- BEGIN TOP NAVIGATION MENU -->
                    <div class="top-menu">
                        <ul class="nav navbar-nav pull-right">
                            <li class="dropdown dropdown-user">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <img alt="" class="img-circle" src="assets/layouts/layout/img/avatar3_small.jpg" />
                                    <span class="username username-hide-on-mobile">
                                        {{Auth::user()->first_name}} {{Auth::user()->last_name}}
                                    </span>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-default">
                                    <li>
                                        <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            <i class="icon-key"></i> Log Out </a>
                                    </li>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                    </form>
                                </ul>
                            </li>
                            <!-- END USER LOGIN DROPDOWN -->
                        </ul>
                    </div>
                    <!-- END TOP NAVIGATION MENU -->
                </div>
                <!-- END HEADER INNER -->
            </div>
            <!-- END HEADER -->
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
            <!-- END HEADER & CONTENT DIVIDER -->
            <!-- BEGIN CONTAINER -->
            <div class="page-container">
            <!-- BEGIN SIDEBAR -->
                <div class="page-sidebar-wrapper">

                    <div class="page-sidebar navbar-collapse collapse">
                        <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
                            <li class="sidebar-toggler-wrapper hide">
                                <div class="sidebar-toggler">
                                    <span></span>
                                </div>
                            </li>
                            <li class="sidebar-search-wrapper">
                                <a href="#">
                                            <img src="{{ asset('assets/pages/img/ckc_logotipo.jpg') }}" class="sidebar-search" /> </a> 
                            </li>
                            @if(Auth::user()->isAdmin())
                            <li class="nav-item">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="icon-users"></i>
                                    <span class="title">Users</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item  ">
                                        <a href="{{ route('add-user') }}" class="nav-link ">
                                            <i class="icon-user"></i>
                                            <span class="title">Add User</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="{{ route('users') }}" class="nav-link ">
                                            <i class="glyphicon glyphicon-eye-open"></i>
                                            <span class="title">View Users</span>
                                        </a>
                                    </li>                    
                                </ul>
                            </li>
                            @endif
                            <li class="nav-item">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="fa fa-users"></i>
                                    <span class="title">Clients</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    @if(Auth::user()->isAdmin())
                                    <li class="nav-item  ">
                                        <a href="{{ route('add-client') }}" class="nav-link ">
                                            <i class="fa fa-plus-circle"></i>
                                            <span class="title">Add Client</span>
                                        </a>
                                    </li>                                    
                                    <li class="nav-item  ">
                                        <a href="{{ route('clients') }}" class="nav-link ">
                                            <i class="glyphicon glyphicon-eye-open"></i>
                                            <span class="title">View Clients</span>
                                        </a>
                                    </li>  
                                    @endif 
                                    @if(Auth::user()->isUser())
                                    <li class="nav-item  ">
                                        <a href="{{ route('user-clients') }}" class="nav-link ">
                                            <i class="glyphicon glyphicon-eye-open"></i>
                                            <span class="title">View Clients</span>
                                        </a>
                                    </li>  
                                    @endif                 
                                </ul>
                            </li>
                            @if(Auth::user()->isAdmin())
                            <li class="nav-item">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="glyphicon glyphicon-star"></i>
                                    <span class="title">Allergy Icons</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item">
                                        <a href="{{ route('add-aicon') }}" class="nav-link ">
                                            <i class="fa fa-plus-circle"></i>
                                            <span class="title">Add Allergy Icon</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{ route('aicons') }}" class="nav-link ">
                                            <i class="glyphicon glyphicon-eye-open"></i>
                                            <span class="title">View Allergy Icons</span>
                                        </a>
                                    </li>                    
                                </ul>
                            </li>
                            @endif
                        </ul>
                    </div>
                    <!-- END SIDEBAR -->
                </div>
                <!-- END SIDEBAR -->     
                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <!-- BEGIN PAGE HEADER-->
                        <!-- BEGIN PAGE BAR -->
                        @yield('content')    
                    </div>
                    <!-- END CONTENT BODY -->
                </div>
                <!-- END CONTENT -->            
            </div>
            <!-- END CONTAINER -->    

                        <!-- BEGIN FOOTER -->
            <div class="page-footer">
                <div class="page-footer-inner"> 2018 &copy; Application for Cyprus Kiosk Company by
                    <a target="_blank" href="http://eclecticsolutions.in">Eclectic Solutions</a> 
                </div>
                <div class="scroll-to-top">
                    <i class="icon-arrow-up"></i>
                </div>
            </div>
            <!-- END FOOTER -->
        </div>
        
        <script src="{{ asset('assets/global/plugins/jquery.min.js') }}"></script>
        <script src="{{ asset('assets/global/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('assets/global/plugins/js.cookie.min.js') }}"></script>
        <script src="{{ asset('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
        <script src="{{ asset('assets/global/plugins/jquery.blockui.min.js') }}"></script>
        <script src="{{ asset('assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}"></script>
        <script src="{{ asset('assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js') }}"></script>
        <script src="{{ asset('assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js') }}"></script>
        <script src="{{ asset('assets/global/plugins/select2/js/select2.full.min.js') }}"></script>
        <script src="{{ asset('assets/global/scripts/app.min.js') }}"></script>
<!--         <script src="{{ asset('assets/pages/scripts/esol-js.js') }}"></script>
 -->        <script src="{{ asset('assets/main.js') }}"></script>
        <script src="{{ asset('assets/layouts/layout/scripts/layout.min.js') }}"></script>
        <script src="{{ asset('assets/layouts/layout/scripts/demo.min.js') }}"></script>

        <script type="text/javascript">
            $(document).ready(function(){
                $("#template_form").submit(function (e) {
                    var scroll_id = '';
                    $(".allergy_icon_select").each(function(){
                        var values = $(this).val();
                        if(values != null){
                            var values_arr = values.toString().split(',');
                            if(values_arr.length > 10){
                                var id = $(this).attr('id').split('_');
                                $('#allergy_icon_error_'+id[3]).html('Select Less than or equal to 10 options');
                                e.preventDefault();
                                if(scroll_id == ''){
                                    scroll_id = id[3];
                                }
                            }   
                        }
                    });
                    $('html, body').animate({
                        scrollTop: $('#template_box_id_'+scroll_id).offset().top
                    }, 500);
                });
            });
        </script>
    </body>

</html>