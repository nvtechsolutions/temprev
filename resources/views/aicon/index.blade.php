@extends('layouts.app')

@section('content')
<div class="page-bar">				
	<ul class="page-breadcrumb">
		<li>
			<a href="{{ route('home') }}">Home</a>
			<i class="fa fa-circle"></i>
		</li>
		<li>
			<a href="{{ route('aicons') }}">Allergy Icons</a>
			<i class="fa fa-circle"></i>
		</li>
		<li>
			<span>View Allergy Icons</span>
		</li>
	</ul>
</div>
<!-- END PAGE BAR -->
<!-- BEGIN PAGE TITLE-->
<h1 class="page-title"> View Allergy Icons
	<small>Here you can view all allergy icons</small>
</h1>
<!-- END PAGE TITLE-->
<!-- END PAGE HEADER-->
<!--<div class="note note-info">
	<p> Show operation messages here </p>
</div> -->

<div class="row">
	<div class="col-md-offset-3 col-md-6">
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-eye"></i>
					View Clients
				</div>		
				<div class="actions">	
					@if(Auth::user()->isAdmin())	
					<a href="{{ route('add-aicon') }}" class="btn default btn-sm">
						<i class="fa fa-plus"></i> Add Allergy Icon
					</a>
					@endif						
				</div>
			</div>
			<div class="portlet-body">
				<table class="table table-striped table-bordered table-hover" id="view_user_table">
					<thead>
						<tr>
							<th> Serial No</th>
							<th> Name </th>
							<th> Actions </th>
						</tr>
					</thead>
					<tbody>
						@foreach($aicons as $aicon)
						<tr class="odd gradeX">
							<td>{{$aicon->id}}</td>
							<td>{{$aicon->name}}</td>
							<td>
								@if(Auth::user()->isAdmin())
								<a href="{{ route('edit-aicon', ['id' => $aicon->id]) }}" class="btn btn-outline btn-circle btn-xs purple">
									<i class="fa fa-edit"></i> Edit </a>
								<a href="{{ route('delete-aicon', ['id' => $aicon->id]) }}" class="btn btn-outline btn-circle btn-xs red delete_entity" data-toggle="modal" data-uid="1">
									<i class="fa fa-trash"></i> Delete </a>
								@endif
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
				
				<div class="modal fade" id="delete_entity_modal" tabindex="-1" role="basic" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
								<h4 class="modal-title bold">Delete</h4>
							</div>
							<div class="modal-body">
								<form action="#" class="horizontal-form" id="delete_entity_form" role="form" method="get">
									<div class="form-body">
										<div class="row">
											<div class="col-md-12">
												<p>Are you sure you want to delete?</p>
											</div>                                      
										</div>
									</div>
									<div class="form-actions right" style="text-align:right;">
										<button type="submit" class="btn btn-lg red-flamingo" id="delete_entity_confirm"><i class="fa fa-check"></i> Yes</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
