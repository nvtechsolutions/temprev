@extends('layouts.app')

@section('content')
<?php 
$page_title = (isset($aicon) ? 'Edit Allergy Icon' : 'Add Allergy Icon');
$page_message = (isset($aicon) ? 'Here you can edit allergy icon' : 'Here you can create a new allergy icon');
$name = (isset($aicon->name) ? $aicon->name : old('name'));
$icon_file = (isset($aicon->icon_file) ? $aicon->icon_file : '');
$id = (isset($aicon->id) ? $aicon->id : '');
?>

<div class="page-bar">
	<ul class="page-breadcrumb">
		<li>
			<a href="{{ route('home') }}">Home</a>
			<i class="fa fa-circle"></i>
		</li>
		<li>
			<a href="{{ route('aicons') }}">Allergy Icons</a>
			<i class="fa fa-circle"></i>
		</li>
		<li>
			<span>{{$page_title}}</span>
		</li>
	</ul>
</div>

<h1 class="page-title"> {{$page_title}}
	<small>{{$page_message}}</small>
</h1>


<div class="row">
	<div class="col-md-offset-3 col-md-6">
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption">
					<i class="glyphicon glyphicon-user"></i>
					{{$page_title}}
				</div>
			</div>			
			<div class="portlet-body form">
				@if ($errors->any())
				    <div class="alert alert-danger">
				        <ul>
				            @foreach ($errors->all() as $error)
				                <li>{{ $error }}</li>
				            @endforeach
				        </ul>
				    </div>
				@endif
				<form role="form" action="{{ route('update-aicon', ['id' => $id]) }}" method="POST" enctype="multipart/form-data">
					{{ csrf_field() }}
					<div class="form-body">
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label>Icon Name</label>
									<input type="text" class="form-control input-sm" name="name" value="{{$name}}"> 
								</div>
								<div class="form-group">
									<label>Icon Upload</label>
									<input type="file"  name="icon_file" id="iconimage">
								</div>
								@if($icon_file != '')
									<img width="100" height="100" src="{!! url('/allergy_icon/'.$icon_file) !!}"></img>
								@endif
							</div>
						</div>
					</div>
					<div class="form-actions right">
						<a href="{{ route('aicons') }}" type="button" class="btn default">Cancel</a>
						<button type="submit" class="btn blue">Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection