@extends('layouts.app')

@section('content')
<?php 
$page_title = (isset($project) ? 'Edit Project' : 'Add Project');
$page_message = (isset($project) ? 'Here you can edit project' : 'Here you can create a new project');
$client_id = (isset($client_id) ? $client_id : '');
$name = (isset($project->name) ? $project->name : old('name'));
$screen_count = (isset($project->screen_count) ? $project->screen_count : old('screen_count'));
$id = (isset($project->id) ? $project->id : '');
?>

<div class="page-bar">
	<ul class="page-breadcrumb">
		<li>
			<a href="{{ route('home') }}">Home</a>
			<i class="fa fa-circle"></i>
		</li>
		<li>
			<a href="{{ route('clients') }}">Clients</a>
			<i class="fa fa-circle"></i>
		</li>
		<li>
			<a href="{{ route('projects', ['client_id' => $client_id]) }}">Projects</a>
			<i class="fa fa-circle"></i>
		</li>
		<li>
			<span>{{$page_title}}</span>
		</li>
	</ul>
</div>

<h1 class="page-title"> {{$page_title}}
	<small>{{$page_message}}</small>
</h1>


<div class="row">
	<div class="col-md-offset-3 col-md-6">
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption">
					<i class="glyphicon glyphicon-user"></i>
					{{$page_title}}
				</div>
			</div>			
			<div class="portlet-body form">
				@if ($errors->any())
				    <div class="alert alert-danger">
				        <ul>
				            @foreach ($errors->all() as $error)
				                <li>{{ $error }}</li>
				            @endforeach
				        </ul>
				    </div>
				@endif
				<form role="form" action="{{ route('update-project', ['id' => $id]) }}" method="POST">
					{{ csrf_field() }}
					<input type="hidden" name="client_id" value="{{$client_id}}">
					<div class="form-body">
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label>Project Name</label>
									<input type="text" class="form-control input-sm" name="name" value="{{$name}}"> 
								</div>
								<div class="form-group">
									<label>Define a Screen</label>
									<div class="input-group">
										<span class="input-group-addon">1 X</span>
										<?php
										if(isset($project)){
											?>
											<input type="text" disabled="disable" class="form-control input-sm" name="" value="{{$screen_count}}">
											<input type="hidden" name="screen_count" value="{{$screen_count}}">
											<?php
										}else{
											?>
											<input type="text" class="form-control input-sm" name="screen_count" value="{{$screen_count}}">
											<?php
										}
										?>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="form-actions right">
						<a href="{{ route('projects', ['client_id' => $client_id]) }}" type="button" class="btn default">Cancel</a>
						<button type="submit" class="btn blue">Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection