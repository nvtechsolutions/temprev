<?php
if(isset($message)){
	echo $message;
	exit;
}
use \App\Aicon;
?>
<html><head>
    <style type="text/css">
		html,body{
			width: 100%;
			height: 100%;
			margin: 0;
			padding: 0;
			border:0;
		}
		h2,img,table,td
		{
			margin: 0;
			padding: 0;
			border: 0;
		}	
		table {
			table-layout:fixed;
			border-collapse: collapse;
			border-spacing: 0;
		}
		.vid{
			object-fit: fill;
		}
        .horizontalAligns {
            text-align: center;
            margin: 0;
			padding: 0;
			border:0;
            vertical-align: middle;
			width:480px;
			height:272px;
			overflow: hidden;
        }
		.horizontalAligns > video{
			display: block;
			border:0;
			margin:0;
			padding:0; 
		}
        .verticalAligns {
            text-align: center;
            position: relative;
        }

        .vertical {
            writing-mode: vertical-rl;
            position: absolute;
            right: 0;
        }

        .font30 {
            font-size: 30pt;
        }

        .font20 {
            font-size: 20pt;
        }

        .image85 {
            height: 85px;
            width: 85px;
        }

        .image65 {
            height: 65px;
            width: 65px;
        }

        .verticalImg {
            transform: rotate(90deg);
        }

        .tableRow {
			height:272px;
            margin: 0;
			padding: 0;
        }
		    
    </style>
</head>





<body>
	@if($template->url_option == '0')
    <table height="" width="1440"><!-- 1088-->
        <tbody>
        	<?php
        	for($i=0; $i<count($template_boxes); $i++){        		
    			// if($i == 0){
       //  			echo '<tr class="tableRow">';
       //  		}
        		if($i%3 == 0){
        			echo '<tr class="tableRow">';
        		}
        		?>
        		@if($template_boxes[$i]->name=="")
        			<td></td>
        		@endif

        		@if($template_boxes[$i]->name=="Leave It Empty")
        			<td></td>
        		@endif

        		@if($template_boxes[$i]->name=="Upload File Image")
        			<td class="horizontalAligns">
						<img width="480" height="272" src="{!! url('/template/box_image/'.$template_boxes[$i]->file) !!}">
					</td>
        		@endif

        		@if($template_boxes[$i]->name=="Upload File Video")
        			<td class="horizontalAligns">
						<video class="vid" width="480" height="272" autoplay loop>
							<source src="{!! url('/template/box_video/'.$template_boxes[$i]->file) !!}" type="video/mp4">
							Your browser does not support HTML5 video.
						</video>
					</td>
        		@endif

        		@if($template_boxes[$i]->name=="Add Text With a Allergy Icon")
	        		
					<td class="horizontalAligns">
						<h2 class="font30">{{$template_boxes[$i]->text}}</h2>
						<?php
						$allergy_icons = explode(',', $template_boxes[$i]->allergy_icon);
						foreach ($allergy_icons as $allergy_icon) {
							$aicon = Aicon::where('id', $allergy_icon)->first();
							if(isset($aicon) && is_object($aicon)){
								?>
								<img src="{!! url('/allergy_icon/'.$aicon->icon_file) !!}" class="image85">
								<?php	
							}
						}
						?>							
					</td>						
        		@endif	
        		<?php
        		
        		if($i%2 == 0){
        			echo '<e</tr>';
        		}
	        		
        	}
        	if($i%3 == 2){
        		echo '<td class="horizontalAligns">&nbsp;</td>';
        		echo '</tr>';
        	}
        	?>
		</tbody>
	</table>
	@else
		{{$template->url}}
	@endif
</body>
</html>	