@extends('layouts.app')

@section('content')
<?php 
$page_title = (isset($client) ? 'Edit Client' : 'Add Client');
$page_message = (isset($client) ? 'Here you can edit client' : 'Here you can create a new client');
$name = (isset($client->name) ? $client->name : old('name'));
$id = (isset($client->id) ? $client->id : '');
?>

<div class="page-bar">
	<ul class="page-breadcrumb">
		<li>
			<a href="{{ route('home') }}">Home</a>
			<i class="fa fa-circle"></i>
		</li>
		<li>
			<a href="{{ route('clients') }}">Clients</a>
			<i class="fa fa-circle"></i>
		</li>
		<li>
			<span>{{$page_title}}</span>
		</li>
	</ul>
</div>

<h1 class="page-title"> {{$page_title}}
	<small>{{$page_message}}</small>
</h1>


<div class="row">
	<div class="col-md-offset-3 col-md-6">
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption">
					<i class="glyphicon glyphicon-user"></i>
					{{$page_title}}
				</div>
			</div>			
			<div class="portlet-body form">
				@if ($errors->any())
				    <div class="alert alert-danger">
				        <ul>
				            @foreach ($errors->all() as $error)
				                <li>{{ $error }}</li>
				            @endforeach
				        </ul>
				    </div>
				@endif
				<form role="form" action="{{ route('update-client', ['id' => $id]) }}" method="POST">
					{{ csrf_field() }}
					<div class="form-body">
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label>Client Name</label>
									<input type="text" class="form-control input-sm" name="name" value="{{$name}}"> 
								</div>
							</div>
						</div>
					</div>
					<div class="form-actions right">
						<a href="{{ route('clients') }}" type="button" class="btn default">Cancel</a>
						<button type="submit" class="btn blue">Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection