@extends('layouts.app')

@section('content')
<div class="page-bar">				
	<ul class="page-breadcrumb">
		<li>
			<a href="{{ route('home') }}">Home</a>
			<i class="fa fa-circle"></i>
		</li>
		<li>
			<a href="{{ route('users') }}">Users</a>
			<i class="fa fa-circle"></i>
		</li>
		<li>
			<span>View Users</span>
		</li>
	</ul>
</div>
<!-- END PAGE BAR -->
<!-- BEGIN PAGE TITLE-->
<h1 class="page-title"> View Users
	<small>Here you can view all users</small>
</h1>
<!-- END PAGE TITLE-->
<!-- END PAGE HEADER-->
<!--<div class="note note-info">
	<p> Show operation messages here </p>
</div> -->

<div class="row">
	<div class="col-md-offset-1 col-md-10">
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-eye"></i>
					View Users
				</div>		
				<div class="actions">		
					<a href="{{ route('add-user') }}" class="btn default btn-sm">								
					<i class="fa fa-plus"></i> Add User</a>						
				</div>
			</div>
			<div class="portlet-body">
				<table class="table table-striped table-bordered table-hover" id="view_user_table">
					<thead>
						<tr>
							<th> Serial No</th>
							<th> First Name </th>
							<th> Last Name </th>
							<th> Email Address </th>
							<th> Actions </th>
						</tr>
					</thead>
					<tbody>
						@foreach($users as $user)
						<tr class="odd gradeX">
							<td>{{$user->id}}</td>
							<td>{{$user->first_name}}</td>
							<td>{{$user->last_name}}</td>
							<td>{{$user->email}}</td>
							<td>
								<a href="{{ route('edit-user', ['id' => $user->id]) }}" class="btn btn-outline btn-circle btn-xs purple">
									<i class="fa fa-edit"></i> Edit </a>
								<a href="{{ route('delete-user', ['id' => $user->id]) }}" class="btn btn-outline btn-circle btn-xs red delete_entity" data-toggle="modal" data-uid="1">
									<i class="fa fa-trash"></i> Delete </a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
				
				<div class="modal fade" id="delete_entity_modal" tabindex="-1" role="basic" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
								<h4 class="modal-title bold">Delete</h4>
							</div>
							<div class="modal-body">
								<form action="#" class="horizontal-form" id="delete_entity_form" role="form" method="get">
									<div class="form-body">
										<div class="row">
											<div class="col-md-12">
												<p>Are you sure you want to delete?</p>
											</div>                                      
										</div>
									</div>
									<div class="form-actions right" style="text-align:right;">
										<button type="submit" class="btn btn-lg red-flamingo" id="delete_entity_confirm"><i class="fa fa-check"></i> Yes</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
