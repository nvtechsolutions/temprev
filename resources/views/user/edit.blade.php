@extends('layouts.app')

@section('content')
<?php 
$page_title = (isset($user) ? 'Edit User' : 'Add User');
$page_message = (isset($user) ? 'Here you can edit user' : 'Here you can create a new user');
$first_name = (isset($user->first_name) ? $user->first_name : old('first_name'));
$last_name = (isset($user->last_name) ? $user->last_name : old('last_name'));
$email = (isset($user->email) ? $user->email : old('email'));
$id = (isset($user->id) ? $user->id : '');
?>

<div class="page-bar">
	<ul class="page-breadcrumb">
		<li>
			<a href="{{ route('home') }}">Home</a>
			<i class="fa fa-circle"></i>
		</li>
		<li>
			<a href="{{ route('users') }}">Users</a>
			<i class="fa fa-circle"></i>
		</li>
		<li>
			<span>{{$page_title}}</span>
		</li>
	</ul>
</div>

<h1 class="page-title"> {{$page_title}}
	<small>{{$page_message}}</small>
</h1>


<div class="row">
	<div class="col-md-offset-2 col-md-8">
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption">
					<i class="glyphicon glyphicon-user"></i>
					{{$page_title}}
				</div>
			</div>			
			<div class="portlet-body form">
				@if ($errors->any())
				    <div class="alert alert-danger">
				        <ul>
				            @foreach ($errors->all() as $error)
				                <li>{{ $error }}</li>
				            @endforeach
				        </ul>
				    </div>
				@endif
				<form role="form" action="{{ route('update-user', ['id' => $id]) }}" method="POST">
					{{ csrf_field() }}
					<div class="form-body">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>First Name</label>
									<input type="text" class="form-control input-sm" name="first_name" value="{{$first_name}}"> 
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Last Name</label>
									<input type="text" class="form-control input-sm" name="last_name" value="{{$last_name}}">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>Email Address</label>
									<div class="input-group">
										<span class="input-group-addon">
											<i class="fa fa-envelope font-blue"></i>
										</span>
										<input type="text" class="form-control input-sm" name="email" value="{{$email}}">
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Password</label>
									<div class="input-group">
										<span class="input-group-addon">
											<i class="fa fa-user font-blue"></i>
										</span>
										<input type="password" class="form-control input-sm" name="password">
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>Select Clients</label>
									<select multiple="" class="form-control" name="clients[]">
										<?php
										foreach($clients as $client){
											$show = 1;
											$selected = 0;
											if(isset($user_clients)){
												foreach($user_clients as $user_client){	
													if($user_client->client_id == $client->id){
														$selected = 1;
													}
													if($user_client->user_id != $id && $user_client->client_id == $client->id ){
														$show = 0;
													}	
												}
											}
											if($show == 1){
												?>
												<option <?php echo ($selected == 1 ? 'selected' : '')?> value="{{$client->id}}">{{$client->name}}</option>
												<?php	
											}								
										}
										?>
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="form-actions right">
						<a href="{{ route('users') }}" type="button" class="btn default">Cancel</a>
						<button type="submit" class="btn blue">Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection