$(document).ready(function() {
	
	if($("#enable_option").get(0)){
	    if ($("#enable_option").is(":checked")) {
	    	$("#template_url").prop("disabled", false);
	    	$('.template_box').each(function(){
				$(this).prop("disabled", true);
			});
	    }
	    else{
	    	$("#template_url").val("");
	    	$("#template_url").prop("disabled", true);

	    	$('.template_box').each(function(){
				$(this).prop("disabled", false);
			});
	    }

	    $("#enable_option").click(function () {
            if ($(this).is(":checked")) {
                $("#template_url").prop("disabled", false);
				$('.template_box').each(function(){
					$(this).prop("disabled", true);
				});
            } else {
            	$("#template_url").val("");
				$("#template_url").prop("disabled", true);
				$('.template_box').each(function(){
					$(this).prop("disabled", false);
				});
            }
        });
	}

	$('.template_box').change(function () {
		var id = $(this).attr('data-box-id');
		if($(this).val() == 'Leave It Empty'){
			$('#template_box_file_image_'+id).addClass('hide');
			$('#template_box_file_video_'+id).addClass('hide');
			$('#template_box_text_'+id).addClass('hide');
			$('#template_box_allergy_'+id).addClass('hide');
		}
		if($(this).val() == 'Upload File Image'){
			$('#template_box_file_image_'+id).removeClass('hide');
			$('#template_box_file_video_'+id).addClass('hide');
			$('#template_box_text_'+id).addClass('hide');
			$('#template_box_allergy_'+id).addClass('hide');
		}
		if($(this).val() == 'Upload File Video'){
			$('#template_box_file_video_'+id).removeClass('hide');
			$('#template_box_file_image_'+id).addClass('hide');
			$('#template_box_text_'+id).addClass('hide');
			$('#template_box_allergy_'+id).addClass('hide');
		}
		if($(this).val() == 'Add Text With a Allergy Icon'){
			$('#template_box_file_image_'+id).addClass('hide');
			$('#template_box_file_video_'+id).addClass('hide');
			$('#template_box_text_'+id).removeClass('hide');
			$('#template_box_allergy_'+id).removeClass('hide');
		}
	});


	// $("#template_url").prop("disabled", true);
	
	// $("#editTemplate_url").prop("disabled", true);

	// $(function () {
 //        $("#enable_option").click(function () {
 //            if ($(this).is(":checked")) {
 //                $("#template_url").prop("disabled", false);
	// 			$("#template_box1").prop("disabled", true);
	// 			$("#template_box2").prop("disabled", true);
 //            } else {
	// 			$("#template_url").prop("disabled", true);
	// 			$("#template_box1").prop("disabled", false);
	// 			$("#template_box2").prop("disabled", false);
 //            }
 //        });
 //    });
	
	// $(function () {
 //        $("#editEnable_option").click(function () {
 //            if ($(this).is(":checked")) {
 //                $("#editTemplate_url").prop("disabled", false);
	// 			$("#edit_template_box1").prop("disabled", true);
	// 			$("#edit_template_box2").prop("disabled", true);
 //            } else {
	// 			$("#editTemplate_url").prop("disabled", true);
	// 			$("#edit_template_box1").prop("disabled", false);
	// 			$("#edit_template_box2").prop("disabled", false);
 //            }
 //        });
 //    });

	
	
	// $('.delete_client').click(function(e) {
 //        $('#delete_client').modal('toggle');
	// 	$('#delete_client_id').val($(this).data('uid'));
 //    });
	
	$('.delete_entity').click(function(e) {
		e.preventDefault();
		$('#delete_entity_form').attr('action',$(this).attr('href'));
        $('#delete_entity_modal').modal('toggle');
    });

    // $('#delete_entity_confirm').click(function(e){
    
    // });
	
	// $('.delete_project').click(function(e) {
 //        $('#delete_project').modal('toggle');
	// 	$('#delete_project_id').val($(this).data('uid'));
 //    });				
	
	// $('.delete_template').click(function(e) {        		
	// 	$('#delete_template').modal('toggle');				
	// 	$('#delete_template_id').val($(this).data('uid'));    	
	// });
	
	// $('.delete_allergy_icon').click(function(e) {        		
	// 	$('#delete_allergy_icon').modal('toggle');				
	// 	$('#delete_allergy_icon_id').val($(this).data('uid'));    	
	// });
	
});


	jQuery(".sidebar-toggler").click(function(){
		jQuery(".sidebar-search").toggleClass("hide");
	});

	// $("#template_box1").change(function () {
	// 	var value = $(this).val();
	// 	if (value == "Upload File") {
	// 		$(".box1_file").removeClass("hide");
	// 	}
	// 	else{
	// 		$(".box1_file").addClass("hide");
	// 	}
	// 	if (value == "Add Text With a Allergy Icon") {
	// 		$(".box1_allergy").removeClass("hide");
	// 	}
	// 	else{
	// 		$(".box1_allergy").addClass("hide");
	// 	}

	// });
	
	// $("#template_box2").change(function () {
	// 	var value = $(this).val();
	// 	if (value == "Upload File") {
	// 		$(".box2_file").removeClass("hide");
	// 	}
	// 	else{
	// 		$(".box2_file").addClass("hide");
	// 	}
	// 	if (value == "Add Text With a Allergy Icon") {
	// 		$(".box2_allergy").removeClass("hide");
	// 	}
	// 	else{
	// 		$(".box2_allergy").addClass("hide");
	// 	}

	// });
	
	// $("#edit_template_box1").change(function () {
	// 	var value = $(this).val();
	// 	if (value == "Upload File") {
	// 		$(".edit_box1_file").removeClass("hide");
	// 	}
	// 	else{
	// 		$(".edit_box1_file").addClass("hide");
	// 	}
	// 	if (value == "Add Text With a Allergy Icon") {
	// 		$(".edit_box1_allergy").removeClass("hide");
	// 	}
	// 	else{
	// 		$(".edit_box1_allergy").addClass("hide");
	// 	}

	// });
	
	// $("#edit_template_box2").change(function () {
	// 	var value = $(this).val();
	// 	if (value == "Upload File") {
	// 		$(".edit_box2_file").removeClass("hide");
	// 	}
	// 	else{
	// 		$(".edit_box2_file").addClass("hide");
	// 	}
	// 	if (value == "Add Text With a Allergy Icon") {
	// 		$(".edit_box2_allergy").removeClass("hide");
	// 	}
	// 	else{
	// 		$(".edit_box2_allergy").addClass("hide");
	// 	}

	// });


var ComponentsDropdowns = function () {

	var handleMultiSelect = function () {
		
		jQuery('#day_of_the_week').multiSelect();
		
		//Select and Deselect all multiselect for Day of the week
		
		jQuery("#select_all_day_week").click(function(){
			jQuery("#day_of_the_week").multiSelect('select_all');
			return false;
		});
		
		jQuery('#deselect_all_day_week').click(function(){
			jQuery('#day_of_the_week').multiSelect('deselect_all');
			return false;
		});
		
		//End
		
		jQuery('#time_of_the_day').multiSelect({cssClass : 'esol_height'});
		
		//Select and Deselect all multiselect for Time of the Day
		
		jQuery("#select-all-time-day").click(function(){
			jQuery("#time_of_the_day").multiSelect('select_all');
			return false;
		});
		
		jQuery("#deselect-all-time-day").click(function(){
			jQuery("#time_of_the_day").multiSelect('deselect_all');
			return false;
		});
		
		//End
		
		jQuery("#week_of_the_year").multiSelect();
		
		//Select and Deselect all multiselect for Week of the year
		
		jQuery("#select-all-week-year").click(function(){
			jQuery("#week_of_the_year").multiSelect('select_all');
			return false;
		});
		
		jQuery('#deselect-all-week-year').click(function(){
			jQuery("#week_of_the_year").multiSelect('deselect_all');
			return false;
		});
		
		//End
	}

	return {
		//main function to initiate the module
		init: function () {   
			handleMultiSelect();
		}
	};

}();


jQuery(document).ready(function() {    
	ComponentsDropdowns.init(); 
});
