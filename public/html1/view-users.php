<?php
	include 'header.php';
	include 'sidebar.php';
?>
                
	<!-- BEGIN CONTENT -->	
	<div class="page-content-wrapper">	
	<!-- BEGIN CONTENT BODY -->		
		<div class="page-content">			
		<!-- BEGIN PAGE HEADER-->
			<!-- BEGIN PAGE BAR -->		
			<div class="page-bar">				
				<ul class="page-breadcrumb">
					<li>
						<a href="index.html">Home</a>
						<i class="fa fa-circle"></i>
					</li>
					<li>
						<a href="#">Users</a>
						<i class="fa fa-circle"></i>
					</li>
					<li>
						<span>View Users</span>
					</li>
				</ul>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h1 class="page-title"> View Users
				<small>Here you can view all users</small>
			</h1>
			<!-- END PAGE TITLE-->
			<!-- END PAGE HEADER-->
			<!--<div class="note note-info">
				<p> Show operation messages here </p>
			</div> -->

			<div class="row">
				<div class="col-md-offset-1 col-md-10">
					<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-eye"></i>
								View Users
							</div>		
							<div class="actions">		
								<a href="add-user.php" class="btn default btn-sm">								
								<i class="fa fa-plus"></i> Add User</a>						
							</div>
						</div>
						<div class="portlet-body">
							<table class="table table-striped table-bordered table-hover" id="view_user_table">
								<thead>
									<tr>
										<th> Serial No</th>
										<th> Full Name </th>
										<th> User Name </th>
										<th> Email Address </th>
										<th> Client Name </th>
										<th> Actions </th>
									</tr>
								</thead>
								<tbody>
									<tr class="odd gradeX">
										<td>1</td>
										<td>Demo Name 01</td>
										<td>Username 01</td>
										<td>Demo Email 01</td>
										<td>Client Name 01</td>
										<td>
											<a href="edit-user.php" class="btn btn-outline btn-circle btn-xs purple">
												<i class="fa fa-edit"></i> Edit </a>
											<a href="#" class="btn btn-outline btn-circle btn-xs red delete_user" data-toggle="modal" data-uid="1">
												<i class="fa fa-trash"></i> Delete </a>
										</td>
									</tr>
									<tr class="odd gradeX">
										<td>2</td>
										<td>Demo Name 02</td>
										<td>Username 02</td>
										<td>Demo Email 02</td>
										<td>Client Name 02</td>
										<td>
											<a href="edit-user.php" class="btn btn-outline btn-circle btn-xs purple">
												<i class="fa fa-edit"></i> Edit </a>
											<a href="javascript:;" class="btn btn-outline btn-circle btn-xs red">
												<i class="fa fa-trash"></i> Delete </a>
										</td>
									</tr>
									<tr class="odd gradeX">
										<td>3</td>
										<td>Demo Name 03</td>
										<td>Username 03</td>
										<td>Demo Email 03</td>
										<td>Client Name 03</td>
										<td>
											<a href="edit-user.php" class="btn btn-outline btn-circle btn-xs purple">
												<i class="fa fa-edit"></i> Edit </a>
											<a href="javascript:;" class="btn btn-outline btn-circle btn-xs red">
												<i class="fa fa-trash"></i> Delete </a>
										</td>
									</tr>
									<tr class="odd gradeX">
										<td>4</td>
										<td>Demo Name 04</td>
										<td>Username 04</td>
										<td>Demo Email 04</td>
										<td>Client Name 04</td>
										<td>
											<a href="edit-user.php" class="btn btn-outline btn-circle btn-xs purple">
												<i class="fa fa-edit"></i> Edit </a>
											<a href="javascript:;" class="btn btn-outline btn-circle btn-xs red">
												<i class="fa fa-trash"></i> Delete </a>
										</td>
									</tr>
									<tr class="odd gradeX">
										<td>5</td>
										<td>Demo Name 05</td>
										<td>Username 05</td>
										<td>Demo Email 05</td>
										<td>Client Name 05</td>
										<td>
											<a href="edit-user.php" class="btn btn-outline btn-circle btn-xs purple">
												<i class="fa fa-edit"></i> Edit </a>
											<a href="javascript:;" class="btn btn-outline btn-circle btn-xs red">
												<i class="fa fa-trash"></i> Delete </a>
										</td>
									</tr>
								</tbody>
							</table>
							
							<div class="modal fade" id="delete_user" tabindex="-1" role="basic" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
											<h4 class="modal-title bold">Delete User</h4>
										</div>
										<div class="modal-body">
											<form action="#" class="horizontal-form" role="form" method="post">
												<div class="form-body">
													<div class="row">
														<div class="col-md-12">
															<p>Are you sure you want to delete this user?</p>
														</div>                                      
													</div>
												</div>
												<input type="hidden" value="" name="delete_user_id" id="delete_user_id">
												<div class="form-actions right" style="text-align:right;">
													<button type="submit" class="btn btn-lg red-flamingo" name="delete_user"><i class="fa fa-check"></i> Yes</button>
												</div>
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END CONTENT BODY -->
	</div>
	<!-- END CONTENT -->
	
</div>
<!-- END CONTAINER -->

<?php include 'footer.php'; ?>