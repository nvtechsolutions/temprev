<?php
	include 'header.php';
	include 'sidebar.php';
?>             

	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<a href="index.php">Home</a>
						<i class="fa fa-circle"></i>
					</li>
					<li>
						<a href="#">Clients</a>
						<i class="fa fa-circle"></i>
					</li>
					<li>
						<span>Add Project</span>
					</li>
				</ul>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h1 class="page-title"> Add Project
				<small>Here you can create a new project</small>
			</h1>
			<!-- END PAGE TITLE-->
			<!-- END PAGE HEADER-->
			<!--<div class="note note-info">
				<p> Show operation messages here </p>
			</div> -->

			<div class="row">
				<div class="col-md-offset-3 col-md-6">
					<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-plus-circle"></i>
								Add Project
							</div>
						</div>
						<div class="portlet-body form">
							<form role="form" action="#">
								<div class="form-body">
									<div class="row">
										<div class="col-md-12">
											<div class="form-group">
												<label>Project Name</label>
												<input type="text" class="form-control input-sm" name="clientName"> 
											</div>
											<div class="form-group">
												<label>Define a Screen</label>
												<div class="input-group">
													<span class="input-group-addon">1 X</span>
													<input type="text" class="form-control input-sm" name="defineScreen">
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="form-actions right">
									<button type="button" class="btn default">Cancel</button>
									<button type="submit" class="btn blue">Submit</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END CONTENT BODY -->
	</div>
	<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->

<?php include 'footer.php'; ?>