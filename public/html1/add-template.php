<?php
	include 'header.php';
	include 'sidebar.php';
?>
        
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<a href="index.php">Home</a>
						<i class="fa fa-circle"></i>
					</li>
					<li>
						<span>Add Template</span>
					</li>
				</ul>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h1 class="page-title"> Add Template
				<small>Here you can create a new template</small>
			</h1>
			<!-- END PAGE TITLE-->
			<!-- END PAGE HEADER-->
			<!--<div class="note note-info">
				<p> Show operation messages here </p>
			</div> -->

			<div class="row">
				<div class="col-md-offset-3 col-md-6">
					<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								<i class="glyphicon glyphicon-file"></i>
								Add Template
							</div>
						</div>
						<div class="portlet-body form">
							<form role="form" action="#">
								<div class="form-body">
									<div class="row">
										<div class="col-md-12">
											<div class="form-group">
												<label>Template Name</label>
												<input type="text" class="form-control input-sm" name="tamplateName"> 
											</div>
											<div class="form-group">
												<div class="mt-checkbox-list">
													<label class="mt-checkbox mt-checkbox-outline"> Enable URL Option
														<input type="checkbox" value="1" name="test" id="enable_option">
														<span></span>
													</label>
												</div>
											</div>
											<div class="form-group">
												<label>URL</label>
												<input type="text" class="form-control input-sm" name="url" id="template_url"> 
											</div>
											<div class="form-group">
												<label>Box 1</label>
												<select class="form-control" id="template_box1">
													<option value="">Select Option</option>
													<option value="Leave It Empty">Leave It Empty</option>
													<option value="Upload File">Upload a Image</option>
													<option value="Upload File">Upload a Video</option>
													<option value="Add Text With a Allergy Icon">Add Text with Allergy Icons</option>
												</select>
											</div>
											<div class="form-group box1_allergy hide">
												<label>Text</label>
												<input type="text" class="form-control input-sm" name="box1AllergyIcon" id="box1AllergyIcon"> 
											</div>
											<div class="form-group box1_allergy hide">
												<label>Select Allergy Icon</label>
												<select multiple="" class="form-control">
													<option value="Allergy Icon 1">Allergy Icon 1</option>
													<option value="Allergy Icon 2">Allergy Icon 2</option>
													<option value="Allergy Icon 3">Allergy Icon 3</option>
													<option value="Allergy Icon 4">Allergy Icon 4</option>
													<option value="Allergy Icon 5">Allergy Icon 5</option>
												</select>
											</div>
											<div class="form-group box1_file hide">
												<label>File Upload</label>
												<input type="file" id="box1FileUpload">
											</div>
											<div class="form-group">
												<label>Box 2</label>
												<select class="form-control" id="template_box2">
													<option value="">Select Option</option>
													<option value="Leave It Empty">Leave It Empty</option>
													<option value="Upload File">Upload a Image</option>
													<option value="Upload File">Upload a Video</option>
													<option value="Add Text With a Allergy Icon">Add Text With Allergy Icons</option>
												</select>
											</div>
											<div class="form-group box2_allergy hide">
												<label>Text</label>
												<input type="text" class="form-control input-sm" name="box2AllergyIcon" id="box2AllergyIcon"> 
											</div>
											<div class="form-group box2_allergy hide">
												<label>Select Allergy Icon</label>
												<select multiple="" class="form-control">
													<option value="Allergy Icon 1">Allergy Icon 1</option>
													<option value="Allergy Icon 2">Allergy Icon 2</option>
													<option value="Allergy Icon 3">Allergy Icon 3</option>
													<option value="Allergy Icon 4">Allergy Icon 4</option>
													<option value="Allergy Icon 5">Allergy Icon 5</option>
												</select>
											</div>
											<div class="form-group box2_file hide">
												<label>File Upload</label>
												<input type="file" id="box2FileUpload">
											</div>
										</div>
									</div>
								</div>
								<div class="form-actions right">
									<button type="button" class="btn default">Cancel</button>
									<button type="submit" class="btn blue">Submit</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END CONTENT BODY -->
	</div>
	<!-- END CONTENT -->
	
</div>
<!-- END CONTAINER -->

<?php include 'footer.php'; ?>