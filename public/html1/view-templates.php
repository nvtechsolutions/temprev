<?php
	include 'header.php';
	include 'sidebar.php';
?>   
           
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<a href="index.php">Home</a>
						<i class="fa fa-circle"></i>
					</li>
					<li>
						<a href="#">Clients</a>
						<i class="fa fa-circle"></i>
					</li>
					<li>
						<span>View Templates</span>
					</li>
				</ul>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h1 class="page-title"> View Templates
				<small>Here you can view all templates</small>
			</h1>
			<!-- END PAGE TITLE-->
			<!-- END PAGE HEADER-->
			<!--<div class="note note-info">
				<p> Show operation messages here </p>
			</div> -->

			<div class="row">
				<div class="col-md-offset-2 col-md-8">
					<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-eye"></i>
								View Templates
							</div>
							<div class="actions">								
								<a href="add-template.php" class="btn default btn-sm">									
									<i class="fa fa-plus"></i> Add Template</a>							
							</div>
						</div>
						<div class="portlet-body">
							<div class="table-scrollable">
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th> Serial No </th>
											<th> Template Name </th>
											<th> Action </th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td> 1 </td>
											<td>Template 01</td>
											<td>
												<a href="template-scheduler.php" class="btn btn-outline btn-circle btn-xs green">
													<i class="glyphicon glyphicon-calendar"></i> Schedule </a>
												<a href="preview" class="btn btn-outline btn-circle btn-xs blue">
													<i class="fa fa-search"></i> Preview </a>
												<a href="edit-template.php" class="btn btn-outline btn-circle btn-xs purple">
													<i class="fa fa-edit"></i> Edit </a>
												<a href="#" class="btn btn-outline btn-circle btn-xs red delete_template" data-toggle="modal" data-uid="1">
													<i class="fa fa-trash"></i> Delete </a>
												
											</td>
										</tr>
										<tr>
											<td> 2 </td>
											<td>Template 02</td>
											<td>
												<a href="template-scheduler.php" class="btn btn-outline btn-circle btn-xs green">
													<i class="glyphicon glyphicon-calendar"></i> Schedule </a>
												<a href="preview" class="btn btn-outline btn-circle btn-xs blue">
													<i class="fa fa-search"></i> Preview </a>
												<a href="edit-template.php" class="btn btn-outline btn-circle btn-xs purple">
													<i class="fa fa-edit"></i> Edit </a>
												<a href="javascript:;" class="btn btn-outline btn-circle btn-xs red">
													<i class="fa fa-trash"></i> Delete </a>
											</td>
										</tr>
										<tr>
											<td> 3 </td>
											<td>Template 03</td>
											<td>
												<a href="template-scheduler.php" class="btn btn-outline btn-circle btn-xs green">
													<i class="glyphicon glyphicon-calendar"></i> Schedule </a>
												<a href="preview" class="btn btn-outline btn-circle btn-xs blue">
													<i class="fa fa-search"></i> Preview </a>
												<a href="edit-template.php" class="btn btn-outline btn-circle btn-xs purple">
													<i class="fa fa-edit"></i> Edit </a>
												<a href="javascript:;" class="btn btn-outline btn-circle btn-xs red">
													<i class="fa fa-trash"></i> Delete </a>
											</td>
										</tr>
										<tr>
											<td> 4 </td>
											<td>Template 04</td>
											<td>
												<a href="template-scheduler.php" class="btn btn-outline btn-circle btn-xs green">
													<i class="glyphicon glyphicon-calendar"></i> Schedule </a>
												<a href="preview" class="btn btn-outline btn-circle btn-xs blue">
													<i class="fa fa-search"></i> Preview </a>
												<a href="edit-template.php" class="btn btn-outline btn-circle btn-xs purple">
													<i class="fa fa-edit"></i> Edit </a>
												<a href="javascript:;" class="btn btn-outline btn-circle btn-xs red">
													<i class="fa fa-trash"></i> Delete </a>
											</td>
										</tr>
										<tr>
											<td> 5 </td>
											<td>Template 05</td>
											<td>
												<a href="template-scheduler.php" class="btn btn-outline btn-circle btn-xs green">
													<i class="glyphicon glyphicon-calendar"></i> Schedule </a>
												<a href="preview" class="btn btn-outline btn-circle btn-xs blue">
													<i class="fa fa-search"></i> Preview </a>
												<a href="edit-template.php" class="btn btn-outline btn-circle btn-xs purple">
													<i class="fa fa-edit"></i> Edit </a>
												<a href="javascript:;" class="btn btn-outline btn-circle btn-xs red">
													<i class="fa fa-trash"></i> Delete </a>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
							
							<div class="modal fade" id="delete_template" tabindex="-1" role="basic" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
											<h4 class="modal-title bold">Delete Template</h4>
										</div>
										<div class="modal-body">
											<form action="#" class="horizontal-form" role="form" method="post">
												<div class="form-body">
													<div class="row">
														<div class="col-md-12">
															<p>Are you sure you want to delete this template?</p>
														</div>                                      
													</div>
												</div>
												<input type="hidden" value="" name="delete_template_id" id="delete_template_id">
												<div class="form-actions right" style="text-align:right;">
													<button type="submit" class="btn btn-lg red-flamingo" name="delete_template"><i class="fa fa-check"></i> Yes</button>
												</div>
											</form>
										</div>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END CONTENT BODY -->
	</div>
	<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->

<?php include 'footer.php'; ?>