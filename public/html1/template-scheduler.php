<?php
	include 'header.php';
	include 'sidebar.php';
?>             
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<a href="index.php">Home</a>
						<i class="fa fa-circle"></i>
					</li>
					<li>
						<span>Template Scheduler</span>
					</li>
				</ul>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h1 class="page-title"> Template Scheduler
				<small>Here you can view template scheduler</small>
			</h1>
			<!-- END PAGE TITLE-->
			<!-- END PAGE HEADER-->
			<div class="note note-warning">
				<p> Show operation messages here </p>
			</div>

			<div class="row">
				<div class="col-md-offset-2 col-md-8">
					<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								<i class="glyphicon glyphicon-calendar"></i>
								Template Scheduler
							</div>
						</div>
						<div class="portlet-body form">
							<form action="#" class="form-horizontal form-row-seperated">
								<div class="form-body">
									<div class="form-group">
										<div class="col-md-4">
											<label class="control-label">Day of the Week</label>
											<p class="help-block">(Only display on the following days of the week)</p>
										</div>
										<div class="col-md-8">
											<a href="" id="select_all_day_week">Select All</a> / 
											<a href="" id="deselect_all_day_week">Deselect All</a>
											<select multiple="multiple" class="multi-select" id="day_of_the_week" name="day_of_the_week[]">
												<option value="monday">Monday</option>
												<option value="tuesday">Tuesday</option>
												<option value="wednesday">Wednesday</option>
												<option value="thursday">Thursday</option>
												<option value="friday">Friday</option>
												<option value="saturday">Saturday</option>
												<option value="sunday">Sunday</option>
											</select>
										</div>
									</div>
									<div class="form-group">
										<div class="col-md-4">
											<label class="control-label">Time of the Day</label>
											<p class="help-block">(Only display at the following times of the day)</p>
										</div>
										<div class="col-md-8">
											<a href="" id="select-all-time-day">Select All</a> / 
											<a href="" id="deselect-all-time-day">Deselect All</a>
											<select multiple="multiple" class="multi-select" id="time_of_the_day" name="time_of_the_day[]">
												<option value="0:00">0:00</option>
												<option value="0:15">0:15</option>
												<option value="0:30">0:30</option>
												<option value="0:45">0:45</option>
												<option value="1:00">1:00</option>
												<option value="1:15">1:15</option>
												<option value="1:30">1:30</option>
												<option value="1:45">1:45</option>
												<option value="2:00">2:00</option>
												<option value="2:15">2:15</option>
												<option value="2:30">2:30</option>
												<option value="2:45">2:45</option>
												<option value="3:00">3:00</option>
												<option value="3:15">3:15</option>
												<option value="3:30">3:30</option>
												<option value="3:45">3:45</option>
												<option value="4:00">4:00</option>
												<option value="4:15">4:15</option>
												<option value="4:30">4:30</option>
												<option value="4:45">4:45</option>
												<option value="5:00">5:00</option>
												<option value="5:15">5:15</option>
												<option value="5:30">5:30</option>
												<option value="5:45">5:45</option>
												<option value="6:00">6:00</option>
												<option value="6:15">6:15</option>
												<option value="6:30">6:30</option>
												<option value="6:45">6:45</option>
												<option value="7:00">7:00</option>
												<option value="7:15">7:15</option>
												<option value="7:30">7:30</option>
												<option value="7:45">7:45</option>
												<option value="8:00">8:00</option>
												<option value="8:15">8:15</option>
												<option value="8:30">8:30</option>
												<option value="8:45">8:45</option>
												<option value="9:00">9:00</option>
												<option value="9:15">9:15</option>
												<option value="9:30">9:30</option>
												<option value="9:45">9:45</option>	
												<option value="10:00">10:00</option>
												<option value="10:15">10:15</option>
												<option value="10:30">10:30</option>
												<option value="10:45">10:45</option>
												<option value="11:00">11:00</option>
												<option value="11:15">11:15</option>
												<option value="11:30">11:30</option>
												<option value="11:45">11:45</option>
												<option value="12:00">12:00</option>
												<option value="12:15">12:15</option>
												<option value="12:30">12:30</option>
												<option value="12:45">12:45</option>
												<option value="13:00">13:00</option>
												<option value="13:15">13:15</option>
												<option value="13:30">13:30</option>
												<option value="13:45">13:45</option>
												<option value="14:00">14:00</option>
												<option value="14:15">14:15</option>
												<option value="14:30">14:30</option>
												<option value="14:45">14:45</option>
												<option value="15:00">15:00</option>
												<option value="15:15">15:15</option>
												<option value="15:30">15:30</option>
												<option value="15:45">15:45</option>
												<option value="16:00">16:00</option>
												<option value="16:15">16:15</option>
												<option value="16:30">16:30</option>
												<option value="16:45">16:45</option>
												<option value="17:00">17:00</option>
												<option value="17:15">17:15</option>
												<option value="17:30">17:30</option>
												<option value="17:45">17:45</option>
												<option value="18:00">18:00</option>
												<option value="18:15">18:15</option>
												<option value="18:30">18:30</option>
												<option value="18:45">18:45</option>
												<option value="19:00">19:00</option>
												<option value="19:15">19:15</option>
												<option value="19:30">19:30</option>
												<option value="19:45">19:45</option>
												<option value="20:00">20:00</option>
												<option value="20:15">20:15</option>
												<option value="20:30">20:30</option>
												<option value="20:45">20:45</option>
												<option value="21:00">21:00</option>
												<option value="21:15">21:15</option>
												<option value="21:30">21:30</option>
												<option value="21:45">21:45</option>
												<option value="22:00">22:00</option>
												<option value="22:15">22:15</option>
												<option value="22:30">22:30</option>
												<option value="22:45">22:45</option>
												<option value="23:00">23:00</option>
												<option value="23:15">23:15</option>
												<option value="23:30">23:30</option>
												<option value="23:45">23:45</option>
											</select>
										</div>
									</div>
									<div class="form-group last">
										<div class="col-md-4">
											<label class="control-label">Week of the Year</label>
											<p class="help-block">(Only display on the following weeks of the year)</p>
										</div>
										<div class="col-md-8">
											<a href="" id="select-all-week-year">Select All</a> / 
											<a href="" id="deselect-all-week-year">Deselect All</a>
											<select multiple="multiple" class="multi-select" id="week_of_the_year" name="week_of_the_year[]">
												<option value="Week 1">Week 1</option>
												<option value="Week 2">Week 2</option>
												<option value="Week 3">Week 3</option>
												<option value="Week 4">Week 4</option>
												<option value="Week 5">Week 5</option>
												<option value="Week 6">Week 6</option>
												<option value="Week 7">Week 7</option>
												<option value="Week 8">Week 8</option>
												<option value="Week 9">Week 9</option>
												<option value="Week 10">Week 10</option>
												<option value="Week 11">Week 11</option>
												<option value="Week 12">Week 12</option>
												<option value="Week 13">Week 13</option>
												<option value="Week 14">Week 14</option>
												<option value="Week 15">Week 15</option>
												<option value="Week 16">Week 16</option>
												<option value="Week 17">Week 17</option>
												<option value="Week 18">Week 18</option>
												<option value="Week 19">Week 19</option>
												<option value="Week 20">Week 20</option>
												<option value="Week 21">Week 21</option>
												<option value="Week 22">Week 22</option>
												<option value="Week 23">Week 23</option>
												<option value="Week 24">Week 24</option>
												<option value="Week 25">Week 25</option>
												<option value="Week 26">Week 26</option>
												<option value="Week 27">Week 27</option>
												<option value="Week 28">Week 28</option>
												<option value="Week 29">Week 29</option>
												<option value="Week 30">Week 30</option>
												<option value="Week 31">Week 31</option>
												<option value="Week 32">Week 32</option>
												<option value="Week 33">Week 33</option>
												<option value="Week 34">Week 34</option>
												<option value="Week 35">Week 35</option>
												<option value="Week 36">Week 36</option>
												<option value="Week 37">Week 37</option>
												<option value="Week 38">Week 38</option>
												<option value="Week 39">Week 39</option>
												<option value="Week 40">Week 40</option>
												<option value="Week 41">Week 41</option>
												<option value="Week 42">Week 42</option>
												<option value="Week 43">Week 43</option>
												<option value="Week 44">Week 44</option>
												<option value="Week 45">Week 45</option>
												<option value="Week 46">Week 46</option>
												<option value="Week 47">Week 47</option>
												<option value="Week 48">Week 48</option>
												<option value="Week 49">Week 49</option>
												<option value="Week 50">Week 50</option>
												<option value="Week 51">Week 51</option>
												<option value="Week 52">Week 52</option>
												<option value="Week 53">Week 53</option>
											</select>
										</div>
									</div>
								</div>
								<div class="form-actions right">
									<button type="button" class="btn default">Cancel</button>
									<button type="submit" class="btn blue">Submit</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END CONTENT BODY -->
	</div>
	<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<?php include 'footer.php'; ?>