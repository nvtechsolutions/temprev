<?php
	include 'header.php';
	include 'sidebar.php';
?>
        
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<a href="index.html">Home</a>
						<i class="fa fa-circle"></i>
					</li>
					<li>
						<a href="#">Users</a>
						<i class="fa fa-circle"></i>
					</li>
					<li>
						<span>Add User</span>
					</li>
				</ul>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h1 class="page-title"> Add User
				<small>Here you can create a new user</small>
			</h1>
			<!-- END PAGE TITLE-->
			<!-- END PAGE HEADER-->
			<!--<div class="note note-info">
				<p> Show operation messages here </p>
			</div> -->

			<div class="row">
				<div class="col-md-offset-2 col-md-8">
					<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								<i class="glyphicon glyphicon-user"></i>
								Add User
							</div>
						</div>
						<div class="portlet-body form">
							<form role="form" action="#">
								<div class="form-body">
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label>First Name</label>
												<input type="text" class="form-control input-sm" name="fullName"> 
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label>Last Name</label>
												<input type="text" class="form-control input-sm" name="userName">
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label>Email Address</label>
												<div class="input-group">
													<span class="input-group-addon">
														<i class="fa fa-envelope font-blue"></i>
													</span>
													<input type="text" class="form-control input-sm" name="emailAddress">
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label>Password</label>
												<div class="input-group">
													<span class="input-group-addon">
														<i class="fa fa-user font-blue"></i>
													</span>
													<input type="password" class="form-control input-sm" name="password">
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label>Select Clients</label>
												<select multiple="" class="form-control" name="userClientName">
													<option>Client 1</option>
													<option>Client 2</option>
													<option>Client 3</option>
													<option>Client 4</option>
													<option>Client 5</option>
												</select>
											</div>
										</div>
									</div>
								</div>
								<div class="form-actions right">
									<button type="button" class="btn default">Cancel</button>
									<button type="submit" class="btn blue">Submit</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END CONTENT BODY -->
	</div>
	<!-- END CONTENT -->
	
</div>
<!-- END CONTAINER -->

<?php include 'footer.php'; ?>