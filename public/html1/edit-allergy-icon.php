<?php
	include 'header.php';
	include 'sidebar.php';
?>
        
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<a href="index.php">Home</a>
						<i class="fa fa-circle"></i>
					</li>
					<li>
						<a href="{{route('viewclients')}}">Allergy Icon</a>
						<i class="fa fa-circle"></i>
					</li>
					<li>
						<span>Edit Allergy Icon</span>
					</li>
				</ul>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE--> 
			<h1 class="page-title"> Edit Allergy Icon
				<small>Here you can edit Allergy Icon</small>
			</h1>
			<!-- END PAGE TITLE-->
			<!-- END PAGE HEADER-->
			<!--<div class="note note-info">
				<p> Show operation messages here </p>
			</div> -->

			<div class="row">
				<div class="col-md-offset-3 col-md-6">
					<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								<i class="glyphicon glyphicon-edit"></i>
								Edit Allergy Icon
							</div>
						</div>
						<div class="portlet-body form">
							<form  action="#" method="post" enctype="multipart/form-data">
								<div class="form-body">
									<div class="row">
										<div class="col-md-12">
											<div class="form-group">
												<label>Icon Name</label>
												<input type="text" class="form-control input-sm" name="iconname" required>
											</div>
                                            <div class="form-group">
												<label>Icon Upload</label>
												<input type="file"  name="iconimage" id="iconimage">
											</div>
										</div>
									</div>
								</div>
								<div class="form-actions right">
									<button type="button" class="btn default">Cancel</button>
									<button type="submit" class="btn blue">Submit</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END CONTENT BODY -->
	</div>
	<!-- END CONTENT -->
</div>

<!-- END CONTAINER -->

<?php include 'footer.php'; ?>