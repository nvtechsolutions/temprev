<?php
	include 'header.php';
	include 'sidebar.php';
?>
        
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<a href="index.php">Home</a>
						<i class="fa fa-circle"></i>
					</li>
					<li>
						<a href="#">Allergy Icon</a>
						<i class="fa fa-circle"></i>
					</li>
					<li>
						<span>View Allergy Icons</span>
					</li>
				</ul>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h1 class="page-title"> View Allergy Icons
				<small>Here you can view all allergy icons</small>
			</h1>
			<!-- END PAGE TITLE-->
			<!-- END PAGE HEADER-->
			<!--<div class="note note-info">
				<p> Show operation messages here </p>
			</div> -->

			<div class="row">
				<div class="col-md-offset-2 col-md-8">
					<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-eye"></i>
								View Allergy Icons
							</div>		
							<div class="actions">		
								<a href="add-allergy-icon.php" class="btn default btn-sm">								
								<i class="fa fa-plus"></i> Add Allergy Icon</a>						
							</div>
						</div>
						<div class="portlet-body">
							<table class="table table-striped table-bordered table-hover" id="view_user_table">
								<thead>
									<tr>
										<th> Serial No</th>
										<th> Allergy Icon Name </th>
										<th> Actions </th>
									</tr>
								</thead>
								<tbody>
									<tr class="odd gradeX">
										<td>1</td>
										<td>Demo Name 01</td>
										<td>
											<a href="edit-allergy-icon.php" class="btn btn-outline btn-circle btn-xs purple">
												<i class="fa fa-edit"></i> Edit </a>
											<a href="#" class="btn btn-outline btn-circle btn-xs red delete_allergy_icon" data-toggle="modal" data-uid="1">
												<i class="fa fa-trash"></i> Delete </a>
										</td>
									</tr>
									<tr class="odd gradeX">
										<td>2</td>
										<td>Demo Name 02</td>
										<td>
											<a href="edit-allergy-icon.php" class="btn btn-outline btn-circle btn-xs purple">
												<i class="fa fa-edit"></i> Edit </a>
											<a href="javascript:;" class="btn btn-outline btn-circle btn-xs red">
												<i class="fa fa-trash"></i> Delete </a>
										</td>
									</tr>
									<tr class="odd gradeX">
										<td>3</td>
										<td>Demo Name 03</td>
										<td>
											<a href="edit-allergy-icon.php" class="btn btn-outline btn-circle btn-xs purple">
												<i class="fa fa-edit"></i> Edit </a>
											<a href="javascript:;" class="btn btn-outline btn-circle btn-xs red">
												<i class="fa fa-trash"></i> Delete </a>
										</td>
									</tr>
									<tr class="odd gradeX">
										<td>4</td>
										<td>Demo Name 04</td>
										<td>
											<a href="edit-allergy-icon.php" class="btn btn-outline btn-circle btn-xs purple">
												<i class="fa fa-edit"></i> Edit </a>
											<a href="javascript:;" class="btn btn-outline btn-circle btn-xs red">
												<i class="fa fa-trash"></i> Delete </a>
										</td>
									</tr>
									<tr class="odd gradeX">
										<td>5</td>
										<td>Demo Name 05</td>
										<td>
											<a href="edit-allergy-icon.php" class="btn btn-outline btn-circle btn-xs purple">
												<i class="fa fa-edit"></i> Edit </a>
											<a href="javascript:;" class="btn btn-outline btn-circle btn-xs red">
												<i class="fa fa-trash"></i> Delete </a>
										</td>
									</tr>
								</tbody>
							</table>
							
							<div class="modal fade" id="delete_allergy_icon" tabindex="-1" role="basic" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
											<h4 class="modal-title bold">Delete Allergy Icon</h4>
										</div>
										<div class="modal-body">
											<form action="#" class="horizontal-form" role="form" method="post">
												<div class="form-body">
													<div class="row">
														<div class="col-md-12">
															<p>Are you sure you want to delete this allergy icon?</p>
														</div>                                      
													</div>
												</div>
												<input type="hidden" value="" name="delete_allergy_icon_id" id="delete_allergy_icon_id">
												<div class="form-actions right" style="text-align:right;">
													<button type="submit" class="btn btn-lg red-flamingo" name="delete_allergy_icon"><i class="fa fa-check"></i> Yes</button>
												</div>
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END CONTENT BODY -->
	</div>
	<!-- END CONTENT -->
</div>

<!-- END CONTAINER -->

<?php include 'footer.php'; ?>