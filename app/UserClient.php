<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserClient extends Model
{
    protected $table = 'users_clients';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id','client_id'
    ];
}
