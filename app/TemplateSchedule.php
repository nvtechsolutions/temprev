<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TemplateSchedule extends Model
{
    protected $table = 'template_schedule';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'project_id','template_id','entity','value'
    ];
}
