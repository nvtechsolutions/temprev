<?php

namespace App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;

class StoreProject extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        // $unique_on_update_name = ($request->segment(2) == '' ? '' : ',id,'.$request->segment(2).'' );
        $required_screencount_on_update = ($request->segment(2) == '' ? 'required|' : 'nullable|');
        return [
            'client_id' => 'required',
            // 'name' => 'required|max:255|unique:projects'.$unique_on_update_name.'',
            'name' => 'required|max:255|',
            'screen_count' => $required_screencount_on_update.'|numeric|digits_between:1,15',
        ];
    }

    public function messages()
    {
        return [
            'client_id.required' => 'The Client field is required.',

            'name.required' => 'The Name field is required.',
            'name.max' => 'The Name can have maximum 255 Characters.',
            'name.unique' => 'This Project name is already taken.',

            'screen_count.required' => 'The Screen Count field is required.',
            'screen_count.numeric' => 'The Screen Count field should be numeric.',
            'screen_count.digits_between' => 'The Screen Count should be greater than 0 and less than 15.',
        ];
    }
}
