<?php

namespace App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;

class StoreUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $unique_on_update_email = ($request->segment(2) == '' ? '' : ',id,'.$request->segment(2).'' );
        $required_password_on_update = ($request->segment(2) == '' ? 'required|' : 'nullable|');
        return [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'clients' => 'required',
            'email' => 'required|email|unique:users'.$unique_on_update_email.'|max:255',
            'password' => $required_password_on_update.'min:8|max:15',
        ]; 
    }

    public function messages()
    {
        return [
            'first_name.required' => 'The First Name field is required.',
            'first_name.max' => 'The First Name can have maximum 255 Characters.',

            'last_name.required' => 'The Last Name field is required.',
            'last_name.max' => 'The Last Name can have maximum 255 Characters.',

            'clients.required' => 'Please select atleast one client.',

            'email.required' => 'The Email field is required.',
            'email.email' => 'Please enter valid Email address.',
            'email.unique' => 'This Email is already taken.',
            'email.max' => 'The Email can have maximum 255 Characters.',

            'password.required' => 'The Password field is required.',
            'password.min' => 'The Password field should be minimum 8 characters long.',
            'password.max' => 'The Password can not exceed more than 15 characters.',
        ];
    }
}
