<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreTemplateSchedule extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'project_id' => 'required',
            'day_of_the_week' => 'required',
            'time_of_the_day' => 'required',
            'week_of_the_year' => 'required',
        ];
    }


    public function messages()
    {
        return [
            'project_id.required' => 'The Project field is required.',
            'day_of_the_week.required' => 'The Day of the week field is required.',
            'time_of_the_day.required' => 'The Time of the Day field is required.',
            'week_of_the_year.required' => 'The Week of the Year field is required.',
        ];
    }
}
