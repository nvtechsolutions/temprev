<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreTemplate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'project_id' => 'required',
            'name' => 'required|max:255',
            'url' => 'required_if:url_option,1',
            // 'template_box_allergy[]' => 'array|between:1,2'
        ];
    }

    public function messages()
    {
        return [
            'project_id.required' => 'The Project field is required.',

            'name.required' => 'The Name field is required.',
            'name.max' => 'The Name can have maximum 255 Characters.',

            'url.required_if' => 'Please enter URL.',

            // 'template_box_allergy[].between' => 'Max should be 3',            
        ];
    }
}
