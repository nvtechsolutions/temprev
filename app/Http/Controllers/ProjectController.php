<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use \App\Project;
use \App\Client;
use \App\Template;
use \App\TemplateBox;
use \App\TemplateSchedule;
use \App\Http\Requests\StoreProject;
use DateTime;


class ProjectController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($client_id)
    {
        $projects = Project::where('client_id', $client_id)->get();
        foreach ($projects as $project) {
          $client = Client::where('id', $project->client_id)->first();
          $project->preview_url = "/".$client->alt_name."/".$project->alt_name;
        }
        return view('project.index', ['projects' => $projects, 'client_id' => $client_id]);
    }

    public function add($client_id)
    {
        return view('project.edit',['client_id' => $client_id]);     
    }


    public function edit($client_id,$id)
    {
        $project = Project::find($id);
        return view('project.edit', ['project' => $project,'client_id' => $client_id]);     
    }

    public function update(StoreProject $request, $id = 0)
    {
      $alt_name = preg_replace('/\s+/', '_', $request->get('name'));
      if($id == 0){
          $project = new Project([
            'client_id' => $request->get('client_id'),
            'name' => $request->get('name'),
            'screen_count' => $request->get('screen_count'),
            'alt_name' => $alt_name,
          ]);
          $project->save();
          
      }
      else{
          $project = Project::find($id);
          $project->client_id = $request->get('client_id');
          $project->name = $request->get('name');
          $project->screen_count = $request->get('screen_count');
          $project->alt_name = $alt_name;
          $project->save();
      }

      return redirect('/projects/'.$request->get('client_id'));
    }


    public function delete($client_id,$id)
    {
        $project = Project::find($id);
        $project->delete();
        return redirect('/projects/'.$client_id);
    }

    public function preview($client_name, $project_name){
      $client = Client::where('alt_name', $client_name)->first();
      if(!empty($client)){
        $project = Project::where('alt_name', $project_name)->where('client_id', $client->id)->first();
        if(!empty($project)){
          $template = $this->getTemplateToShow($project->id);
          if(!empty($template)){
            $template_boxes = TemplateBox::where('template_id', $template->id)->get();
            return view('project.preview',['template'=>$template,'template_boxes' => $template_boxes]);
          }else{
            return view('project.preview',['message'=>'<h2>No template to show at the moment</h2>']);
          }
        }else{
          echo 'Project Does not exist<br/>';
        }
      }else{
        echo 'Client Does not exist<br/>';
      }
    }

    public function getTemplateToShow($project_id){
      
      $dowMap = array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
      $dow_numeric = date('w');
      $day = $dowMap[$dow_numeric];


      $now = new DateTime();
      $week = $now->format("W");

      $time = $now->format("H:i");
      $week = "Week $week";

      $templates = Template::where('project_id', $project_id)->get();
      
      foreach ($templates as $template) {
        $template_schedules = TemplateSchedule::where('template_id', $template->id)->get();
        $template_day = false;
        $template_time = false;
        $template_week = false;
        foreach ($template_schedules as $template_schedule) {
          if($template_schedule->entity == 'DAY' && $template_schedule->value == $day){
            $template_day = true;
          }
          if($template_schedule->entity == 'TIME'){
            $db_time = explode(':', $template_schedule->value);
            $current_time = explode(':', $time);
            if($db_time[0] == $current_time[0]){
              if($current_time[1] >= $db_time[1] && $current_time[1] < $db_time[1]+15){
                $template_time = true; 
              }
            }            
          }
          if($template_schedule->entity == 'WEEK' && $template_schedule->value == $week){
            $template_week = true;
          }

        }

        if($template_day && $template_week && $template_time){
          return $template;
        }
      }

    }
}
