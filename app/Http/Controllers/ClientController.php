<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use \App\Client;
use \App\UserClient;
use \App\Http\Requests\StoreClient;

class ClientController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients = Client::all();
        return view('client.index', ['clients' => $clients]);
    }

    public function userClients()
    {
        $user_clients = UserClient::select('client_id')->where('user_id', Auth::user()->id)->get();
        $client_arr = array();
        if(!empty($user_clients)){
            foreach ($user_clients as $user_client) {
                $client_arr[] = $user_client->client_id;
            }
        }
        $clients = Client::whereIn('id', $client_arr)->get();
        return view('client.index', ['clients' => $clients]);
    }

    public function add()
    {
        return view('client.edit');     
    }


    public function edit($id)
    {
        $client = Client::find($id);
        return view('client.edit', ['client' => $client]);     
    }

    public function update(StoreClient $request, $id = 0)
    {
        $alt_name = preg_replace('/\s+/', '_', $request->get('name'));

        if($id == 0){
            $client = new Client([
              'name' => $request->get('name'),
              'alt_name' => $alt_name,
            ]);
            $client->save();
            
        }
        else{
            $client = Client::find($id);
            $client->name = $request->get('name');
            $client->alt_name = $alt_name;
            $client->save();
        }

        return redirect('/clients');
    }


    public function delete($id)
    {
        $client = Client::find($id);
        $client->delete();
        return redirect('/clients');
    }
}
