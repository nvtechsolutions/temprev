<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use \App\Aicon;
use \App\Http\Requests\StoreAicon;

class AiconController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $aicons = Aicon::all();
        return view('aicon.index', ['aicons' => $aicons]);
    }

    public function add()
    {
        return view('aicon.edit');     
    }


    public function edit($id)
    {
        $aicon = Aicon::find($id);
        return view('aicon.edit', ['aicon' => $aicon]);     
    }

    public function update(StoreAicon $request, $id = 0)
    {
        if($id == 0){
            $aicon = new Aicon([
              'name' => $request->get('name'),
              'icon_file' => '',
            ]);
            $aicon->save();
            
        }
        else{
            $aicon = Aicon::find($id);
            $aicon->name = $request->get('name');
            $aicon->save();
        }


        if($request->hasFile('icon_file')){
	        $extensions = $request->file('icon_file')->getClientOriginalExtension();
	          
	        $file = $aicon->id.'.'.$extensions;

	        $path = $request->file('icon_file')->move(public_path('allergy_icon'), $file);

	        $aicon1 = Aicon::find($aicon->id);
	        $aicon1->icon_file = $file;
	        $aicon1->save();                
      	}

        return redirect('/aicons');
    }


    public function delete($id)
    {
        $aicon = Aicon::find($id);
        $aicon->delete();
        return redirect('/aicons');
    }
}
