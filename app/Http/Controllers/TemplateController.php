<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use \App\Aicon;
use \App\Template;
use \App\Client;
use \App\Project;
use \App\TemplateBox;
use \App\TemplateSchedule;
use \App\Http\Requests\StoreTemplate;
use \App\Http\Requests\StoreTemplateSchedule;

class TemplateController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($client_id,$project_id)
    {
        $templates = Template::where('project_id', $project_id)->get();
        foreach ($templates as $template) {
          $project = Project::where('id', $template->project_id)->first();
          $client = Client::where('id', $project->client_id)->first();
          $template->preview_url = "/".$client->alt_name."/".$project->alt_name."/".$template->alt_name;
        }
        return view('template.index', ['templates' => $templates, 'project_id' => $project_id, 'client_id' => $client_id]);
    }

    public function add($client_id,$project_id)
    {
        $project = Project::find($project_id);
        $aicons = Aicon::orderBy('name', 'asc')->get();
        return view('template.edit',['project_id' => $project_id,'client_id' => $client_id,'screen_count' => $project->screen_count, 'aicons' => $aicons]);     
    }


    public function edit($client_id,$project_id,$id)
    {
        $project = Project::find($project_id);
        $template = Template::find($id);
        $template_boxes = TemplateBox::where('template_id', $template->id)->get();
        $aicons = Aicon::orderBy('name', 'asc')->get();
        return view('template.edit', ['template' => $template,'client_id' => $client_id,'project_id' => $project_id,'screen_count' => $project->screen_count,'template_boxes'=>$template_boxes,'aicons' => $aicons]);     
    }

    public function update(StoreTemplate $request, $client_id, $id = 0)
    {
        $alt_name = preg_replace('/\s+/', '_', $request->get('name'));
        $url_option = (is_null($request->get('url_option')) ? 0 : $request->get('url_option'));
        $url = (is_null($request->get('url')) ? '' : $request->get('url'));
        if($id == 0){            
            $template = new Template([
              'project_id' => $request->get('project_id'),
              'name' => $request->get('name'),
              'url_option' => $url_option,
              'url' => $url,
              'alt_name' => $alt_name,
            ]);
            $template->save();
        }
        else{
            $template = Template::find($id);
            $template->project_id = $request->get('project_id');
            $template->name = $request->get('name');
            $template->url_option = $url_option;
            $template->url = $url;
            $template->alt_name = $alt_name;
            $template->save();
        }
        
        if($url_option == 0){
          //TemplateBox::where('template_id', $template->id)->delete();
          
          foreach ($request->get('template_box') as $key => $request_template_box) {

            $template_box_id = (!isset($request->get('template_box_id')[$key]) ? '' : $request->get('template_box_id')[$key]);
            $name = (!isset($request->get('template_box')[$key]) ? '' : $request->get('template_box')[$key]);
            $text = '';
            $allergy_icon = '';
            $file = '';
            

            if($name == 'Add Text With a Allergy Icon'){
              $text = (!isset($request->get('template_box_text')[$key]) ? '' : $request->get('template_box_text')[$key]);
              $allergy_icons_arr = (is_null($request->get('template_box_allergy')) ? '' : $request->get('template_box_allergy'));
              if(!empty($allergy_icons_arr[$key])){
                foreach ($allergy_icons_arr[$key] as $allergy_icons_a) {
                  $allergy_icon .= ($allergy_icon == '' ? $allergy_icons_a : ','.$allergy_icons_a);
                }
              }
            }

            if($template_box_id == ''){           
              $template_box = new TemplateBox([
                'name' => $name,
                'template_id' => $template->id,
                'text' => $text,
                'allergy_icon' => $allergy_icon,
                'file' => $file,
              ]);
              $template_box->save();
            }else{
              $template_box = TemplateBox::find($template_box_id);
              $template_box->template_id = $template->id;
              $template_box->name = $name;
              $template_box->text = $text;
              $template_box->allergy_icon = $allergy_icon;
              $template_box->save();
            }

            if($name == 'Upload File Image'){
              if($request->hasFile('template_box_file_image_'.$key)){
                $extensions = $request->file('template_box_file_image_'.$key)->getClientOriginalExtension();
                  
                $file = $template_box->id.'.'.$extensions;

                $path = $request->file('template_box_file_image_'.$key)->move(public_path('template/box_image'), $file);

                $template_box1 = TemplateBox::find($template_box->id);
                $template_box1->file = $file;
                $template_box1->save();                
              }
            }

            if($name == 'Upload File Video'){
              if($request->hasFile('template_box_file_video_'.$key)){
                $extensions = $request->file('template_box_file_video_'.$key)->getClientOriginalExtension();
                  
                $file = $template_box->id.'.'.$extensions;

                $path = $request->file('template_box_file_video_'.$key)->move(public_path('template/box_video'), $file);

                $template_box1 = TemplateBox::find($template_box->id);
                $template_box1->file = $file;
                $template_box1->save();
              }
            }
          }
        }

        return redirect('/templates/'.$client_id.'/'.$request->get('project_id'));
    }


    public function delete($client_id,$project_id,$id)
    {
        $template = Template::find($id);
        $template->delete();
        return redirect('/templates/'.$client_id.'/'.$project_id);
    }


    public function preview($client_name, $project_name, $template_name){
      $client = Client::where('alt_name', $client_name)->first();
      if(!empty($client)){
        $project = Project::where('alt_name', $project_name)->where('client_id', $client->id)->first();
        if(!empty($project)){
          $template = Template::where('alt_name', $template_name)->where('project_id', $project->id)->first();
          if(!empty($template)){
            $template_boxes = TemplateBox::where('template_id', $template->id)->get();
            return view('template.preview',['template'=>$template,'template_boxes' => $template_boxes]);

          }else{
            echo 'Template Does not exist<br/>';
          }
        }else{
          echo 'Project Does not exist<br/>';
        }
      }else{
        echo 'Client Does not exist<br/>';
      }
    }

    public function schedule($client_id,$project_id,$id){
      $template = Template::find($id);
      $schedules = TemplateSchedule::where('template_id', $id)->get();
      $project_schedules = TemplateSchedule::where('project_id', $project_id)->where('template_id','!=' ,$id)->get();
      return view('template.schedule', ['client_id' => $client_id,'template' => $template,'schedules' => $schedules,'project_schedules' => $project_schedules]); 
    }


    public function scheduleUpdate(StoreTemplateSchedule $request,$client_id,$id){   

      $project_id = $request->get('project_id');
      $days = $request->get('day_of_the_week');
      $times = $request->get('time_of_the_day');
      $weeks = $request->get('week_of_the_year');

      $schedule_exist = false;
      foreach ($days as $day) {
        foreach ($times as $time) {
          foreach ($weeks as $week) {
            $db_days = TemplateSchedule::where('project_id', $project_id)
                                        ->where('entity', 'DAY')
                                        ->where('value', $day)
                                        ->get();

            $db_times = TemplateSchedule::where('project_id', $project_id)
                                        ->where('entity', 'TIME')
                                        ->where('value', $time)
                                        ->get();

            $db_weeks = TemplateSchedule::where('project_id', $project_id)
                                        ->where('entity', 'WEEK')
                                        ->where('value', $week)
                                        ->get();

            foreach ($db_days as $db_day) {
              foreach ($db_times as $db_time) {
                foreach ($db_weeks as $db_week) {
                  if($db_day->template_id != $id){
                    if($db_day->template_id == $db_time->template_id && $db_day->template_id == $db_week->template_id){
                      $schedule_exist = true;
                    }  
                  }                  
                }
              }
            }
          }
        }
      }

      if($schedule_exist){
        return redirect()->to('schedule-template/'.$client_id.'/'.$project_id.'/'.$id)->withErrors(new \Illuminate\Support\MessageBag(['catch_exception'=>'This schedule is already set for another template for this project. Please select different Day, Time and Week Combination']));
        exit;
      }

      TemplateSchedule::where('template_id', $id)->delete();
      
      if(!empty($days)){
        foreach ($days as $day) {
          $template_schedule = new TemplateSchedule([
            'project_id' => $project_id,
            'template_id' => $id,
            'entity' => 'DAY',
            'value' => $day,
          ]);
          $template_schedule->save();
        }
      }      

      if(!empty($times)){
        foreach ($times as $time) {
          $template_schedule = new TemplateSchedule([
            'project_id' => $project_id,
            'template_id' => $id,
            'entity' => 'TIME',
            'value' => $time,
          ]);
          $template_schedule->save();
        }
      }

      if(!empty($weeks)){
        foreach ($weeks as $week) {
          $template_schedule = new TemplateSchedule([
            'project_id' => $project_id,
            'template_id' => $id,
            'entity' => 'WEEK',
            'value' => $week,
          ]);
          $template_schedule->save();
        }
      }

      return redirect('/templates/'.$client_id.'/'.$request->get('project_id'));

    }

}
