<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use \App\User;
use \App\Client;
use \App\UserClient;
use \App\Http\Requests\StoreUser;
use Illuminate\Support\Facades\Hash;


class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = \App\User::where('role', 1)->get();
        return view('user.index', ['users' => $users]);
    }

    public function add()
    {
        $clients = Client::all();
        $user_clients = UserClient::all();
        return view('user.edit', ['clients' => $clients, 'user_clients' => $user_clients]);     
    }


    public function edit($id)
    {
        $user = User::find($id);
        $clients = Client::all();
        $user_clients = UserClient::all();
        return view('user.edit', ['user' => $user, 'clients' => $clients, 'user_clients' => $user_clients]);     
    }

    public function update(StoreUser $request, $id = 0)
    {
        if($id == 0){
            $user = new User([
              'role' => '1',
              'first_name' => $request->get('first_name'),
              'last_name' => $request->get('last_name'),
              'email' => $request->get('email'),
              'password' => Hash::make($request->get('password')),
            ]);
            $user->save();
            $id = $user->id;
        }
        else{
            $user = User::find($id);
            $user->first_name = $request->get('first_name');
            $user->last_name = $request->get('last_name');
            $user->email = $request->get('email');
            if($request->get('password') != ''){
                $user->password = Hash::make($request->get('password'));
            }
            $user->save();
        }

        //Adding/Updating clients while adding/updating user
        $clients = $request->get('clients');
        UserClient::where('user_id', $id)->delete();
        if(!empty($clients)){
          foreach ($clients as $clientId) {
              $user_client = new UserClient([
                'user_id' => $id,
                'client_id' => $clientId,
              ]);
              $user_client->save();
          }          
        }



        return redirect('/users');
    }


    public function delete($id)
    {
        UserClient::where('user_id', $id)->delete();
        $user = User::find($id);
        $user->delete();
        return redirect('/users');
    }
}
