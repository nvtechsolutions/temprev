<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TemplateBox extends Model
{
    protected $table = 'template_boxes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'template_id','name','file','text','allergy_icon'
    ];
}
