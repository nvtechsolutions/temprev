<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Aicon extends Model
{
    protected $table = 'aicons';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','icon_file'
    ];
}
