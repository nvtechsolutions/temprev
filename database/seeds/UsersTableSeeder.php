<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'role' => '0',
            'first_name' => 'Eclectic',
            'last_name' => 'Solutions',
            'email' => 'ckc1@eclecticsolutions.in',
            'password' => bcrypt('123123123'),
        ]);
    }
}
