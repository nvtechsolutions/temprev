<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('home', 'HomeController@index')->name('home');

Route::middleware(['admin'])->group(function () {

	Route::get('users', 'UserController@index')->name('users');
	Route::get('add-user', 'UserController@add')->name('add-user');
	Route::get('edit-user/{id}', 'UserController@edit')->name('edit-user');
	Route::get('delete-user/{id}', 'UserController@delete')->name('delete-user');
	Route::post('update-user/{id?}', 'UserController@update')->name('update-user');

	Route::get('clients', 'ClientController@index')->name('clients');
	Route::get('add-client', 'ClientController@add')->name('add-client');
	Route::get('edit-client/{id}', 'ClientController@edit')->name('edit-client');
	Route::get('delete-client/{id}', 'ClientController@delete')->name('delete-client');
	Route::post('update-client/{id?}', 'ClientController@update')->name('update-client');


	Route::get('aicons', 'AiconController@index')->name('aicons');
	Route::get('add-aicon', 'AiconController@add')->name('add-aicon');
	Route::get('edit-aicon/{id}', 'AiconController@edit')->name('edit-aicon');
	Route::get('delete-aicon/{id}', 'AiconController@delete')->name('delete-aicon');
	Route::post('update-aicon/{id?}', 'AiconController@update')->name('update-aicon');

});



Route::middleware(['user'])->group(function () {
	Route::get('user-clients', 'ClientController@userClients')->name('user-clients');
});


	Route::get('projects/{client_id}', 'ProjectController@index')->name('projects');
	Route::get('add-project/{client_id}', 'ProjectController@add')->name('add-project');
	Route::get('edit-project/{client_id}/{id}', 'ProjectController@edit')->name('edit-project');
	Route::get('delete-project/{client_id}/{id}', 'ProjectController@delete')->name('delete-project');
	Route::post('update-project/{id?}', 'ProjectController@update')->name('update-project');


	Route::get('templates/{client_id}/{project_id}', 'TemplateController@index')->name('templates');
	Route::get('add-template/{client_id}/{project_id}', 'TemplateController@add')->name('add-template');
	Route::get('edit-template/{client_id}/{project_id}/{id}', 'TemplateController@edit')->name('edit-template');
	Route::get('delete-template/{client_id}/{project_id}/{id}', 'TemplateController@delete')->name('delete-template');
	Route::get('preview-template/{client_id}/{project_id}/{id}', 'TemplateController@delete')->name('preview-template');
	Route::get('schedule-template/{client_id}/{project_id}/{id}', 'TemplateController@schedule')->name('schedule-template');
	Route::get('schedule-update-template/{client_id}/{id}', 'TemplateController@scheduleUpdate')->name('schedule-update-template');
	Route::post('update-template/{client_id}/{id?}', 'TemplateController@update')->name('update-template');




Route::get('{client_name}/{project_name}/{template_name}', 'TemplateController@preview')->name('template-preview');

Route::get('{client_name}/{project_name}', 'ProjectController@preview')->name('project-preview');